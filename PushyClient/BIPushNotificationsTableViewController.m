//
//  BIPushNotificationsTableViewController.m
//  PushyClient
//
//  Created by Jan Galler on 18/02/15.
//  Copyright (c) 2015 byte.it [Kees Galler] GbR. All rights reserved.
//

#import "BIPushNotificationsTableViewController.h"

@interface BIPushNotificationsTableViewController ()

// We need some kind of DataStorage to store received NF. You might already have your own DataStorage, so you wont need this.
@property (strong, nonatomic) NSMutableArray *dataArray;

@end


@implementation BIPushNotificationsTableViewController

// Called very early on launch, much before viewDidLoad will be called
// Note: If we'll get a NF at this early point, we will not be able to update our views, because they don't exist!
// This is important to handle the case of our application beeing closed (not inactive) and being launched by a NF from NFC
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    
    if (self)
    {
        // Initialize DataStorage before (!) we start observing, important!
        self.dataArray = [NSMutableArray new];
        
        // Observe the NSNotificationCenter for PushNotifications during runtime
        [self setUpObservers];
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
     self.clearsSelectionOnViewWillAppear = YES;
    
    // You might want to check if there're currently "undisplayed" objects in your DataStorage that have been added
    // before viewDidLoad was called (as there were no UI, they couldn't be displayed)
}

- (void)setUpObservers
{
    // Register for PushNotifications of your choice. You probably want to handle different kinds of notifications different
    // E.g. You might want to handle New-Article-NF different than Updated-Article NF
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addPushByNotification:) name:BIPUSHY_DID_RECEIVE_ALL_NOTIFICATION object:nil];
}

- (void)dealloc
{
    // It's very (!!!) important to remove self from the list of objects that NSNC will call on a NF
    // Otherwise this will lead into a NullPointerException on new NF
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


#pragma mark - DataSource Management Methods

// Called on new NF
- (void)addPushByNotification:(NSNotification *)nf
{
    // You really should check here whether we've got UI and if not, just add it to your DataStorage and handle displaying at line 45
    
    // Just keep in mind: This NF doesn't contain much information but the information you need to pull everything associated to the NF.
    // E.g. the title might be truncated and just a preview, don't use this in detail, just to tease the user!
    // The Article-ID is given, call WordPress (REST API) by this ID and retrieve every information you normally need for displaying the article.
    // So, for example, use the REST-API-retrieved title instead of the title provided by the NF.
    // To sum up, this might be the right point to load associated information about the article you need for displaying.
    
    // Add the new NF to the TableView
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:self.dataArray.count inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    // Add the new NF to our DataStorage, this is important! You have to store at least the NotificationID of the NF-UserInfo-Object to pass it to the DetailViewController.
    [self.dataArray addObject:[nf userInfo]];
    [self.tableView endUpdates];
    
    // Special handling for NF that caused the application's current launch
    if ([[nf userInfo] objectForKey:BIPUSHY_PUSH_DICT_IS_LAUNCH_PUSH])
    {
        // Wait for 0.5 seconds before we push a new ViewController. You might want to increase this time until your current UI is completly loaded
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^
        {
            // Just push the DetailViewController to display the new article
            [self performSegueWithIdentifier:@"ShowPushDetails" sender:[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:(self.dataArray.count - 1) inSection:0]]];
        });
        
    }
    
}


// Following Methods are just for UI, you might have this implemented already your way, but keep an eye on the DetailViewController

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return self.dataArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [self.dataArray objectAtIndex:indexPath.row];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"DefaultPushCell" forIndexPath:indexPath];
    
    // Configure the cell...
    cell.textLabel.text = [NSString stringWithFormat:@"%@: %@",[dict objectForKey:BIPUSHY_PUSH_DICT_POST_ID], [dict objectForKey:BIPUSHY_PUSH_DICT_ALERT]];
    
    return cell;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Check whether we preparing a segue for our DetailViewController
    if ([segue.identifier isEqualToString:@"ShowPushDetails"])
    {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender]; // Get the IndexPath of the currently tapped row
        BIPushDetailViewController *detailVC = [segue destinationViewController]; // Retrieve YourArticleDetailViewController-instance from the segue
        detailVC.pushData = [self.dataArray objectAtIndex:indexPath.row]; // Assign the notification-dictionary to the pushData-property
    }
    
}


@end
