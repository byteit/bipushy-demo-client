//
//  BISettingsTableViewController.m
//  PushyClient
//
//  Created by Jan Galler on 19/08/15.
//  Copyright (c) 2015 byte.it [Kees Galler] GbR. All rights reserved.
//

#import "BISettingsTableViewController.h"


@implementation BISettingsTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Just to dismiss the Keyboard
    UITapGestureRecognizer *tapToDismiss = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissNumberKeyBoard)];
    [self.view addGestureRecognizer:tapToDismiss];
	
	// Store the WP1 target as demo and make further usage more readable
	self.WP1 = [[BIPushy sharedInstance] getTargetForReadableIdentifier:@"WP1" andTargetIdentifier:@"1"];
	
    // Set UserID
    self.userIDTextField.text = [NSString stringWithFormat:@"%ld", (long)[self.WP1 getUserID]];
    
    // Set global Comment-Subscription switch. The key is always 'subscribes_comments'!
    [self.subscribeCommentsSwitch setOn:[[self.WP1 getMetaValueForKey:@"subscribes_comments"] integerValue] animated:YES];
}


#pragma mark - UI Interaction Methods

// You always should provide this option for any user
- (IBAction)pushClearBadgeButton:(id)sender
{
    // Just an example how to clear the Badge-Counter
    [self.WP1 setBadgeCount:0];
}

// You always should provide this option for any user
- (IBAction)pushSubscribeCommentsSwitch:(id)sender
{
    if(self.subscribeCommentsSwitch.isOn)
    {
        // Turn global comments push on. The key is always 'subscribes_comments'!
		[self.WP1 setMetaValue:[NSNumber numberWithBool:YES] forKey:@"subscribes_comments"];
    }
    else
    {
        // User does not want push for comments, turn it off. The key is always 'subscribes_comments'!
        [self.WP1 setMetaValue:[NSNumber numberWithBool:NO] forKey:@"subscribes_comments"];
    }
}

- (IBAction)pushDone:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^()
    {
        // Do something usefull here
    }];
}

- (void)dismissNumberKeyBoard
{
    [self.userIDTextField resignFirstResponder];
    
    // Keep in mind: This is just a demonstration. In a real app, you would not let a user change
    // his own UserID. This is something you have to 'create' by a user's account/mail or so.
    // Maybe just hash the mail address as integer. The UserID is integer-only!
    if([self.WP1 getUserID] != [self.userIDTextField.text integerValue])
    {
        [self.WP1 setUserID:[self.userIDTextField.text integerValue]];
    }
}



@end
