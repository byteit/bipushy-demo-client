//
//  AppDelegate.m
//  PushyClient
//
//  Created by Jan Galler on 13/02/15.
//  Copyright (c) 2015 byte.it [Kees Galler] GbR. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate


#pragma mark - Constants

// Define the constants you need to use actionable notifications
NSString *const NotificationPostCategoryIdentifier  = @"ARTICLE_ACTIONABLE"; // You should keep this very short, because it has to be included in every payload
NSString *const NotificationPostActionReadIdentifier = @"ACTION_READ";
NSString *const NotificationPostActionMarkReadIdentifier = @"ACTION_MARK_AS_READ";

NSString *const NotificationCommentCategoryIdentifier  = @"COMMENT_ACTIONABLE"; // You should keep this very short, because it has to be included in every payload
NSString *const NotificationCommentActionReplyIdentifier = @"ACTION_REPLY";
NSString *const NotificationCommentActionMuteIdentifier = @"ACTION_MUTE";


#pragma mark - Required Launching Methods

// Called on application-launch
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Initialize Pushy-Library on launch, very important!
    self.pushyLibrary = [BIPushy sharedInstance]; // Yep, this is a singleton. You might not like that, but it works.
	
	// Create a target, meaning here: a source of push notifications, a WordPress instance
	// You do not need to store this anywhere, the library makes it retrieveable by the readableIdentifier specified below
	BITarget *target1 = [[BITarget alloc] initWithBaseURL:@"http://pushy.lets-byte.it"							// Your WordPress-Site's URL with http:// and without trailing-slash
																												// ATTENTION: Network requests might fail due App-Transport-Security if you do not use SSL.
																												// So create an exception to allow insecure calls to this URL, otherwise this whole thing will not work. Search-Keyword: NSAppTransportSecurity
																												// This will not be a problem if you use SSL. ATTENTION: Currently you can not use a different SSL-Port than 443.
										   andAPIEndPoint:@"wp-content/plugins/BIPushy/API/public/index.php/v1" // You won't change that
											 andAppSecret:@"kuw45zxfbuau4wl4e5vzlnq3zdksfjgnso5i"				// Set the target's App-Secret here
											  andTargetID:@"1"													// Set your target's ID here
									andReadableIdentifier:@"WP1"];												// You need to set something here to identify the source/target later, like "MyWordPress-1"
	

	// Create another target, meaning here: a source of push notifications, a WordPress instance
	// You do not need to store this anywhere, the library makes it retrieveable by the readableIdentifier specified below
	BITarget *target2 = [[BITarget alloc] initWithBaseURL:@"http://pushy.lets-byte.it"							// Your WordPress-Site's URL with http:// and without trailing-slash
																												// ATTENTION: Network requests might fail due App-Transport-Security if you do not use SSL.
																												// So create an exception to allow insecure calls to this URL, otherwise this whole thing will not work. Search-Keyword: NSAppTransportSecurity
																												// This will not be a problem if you use SSL. ATTENTION: Currently you can not use a different SSL-Port than 443.
										   andAPIEndPoint:@"wp-content/plugins/BIPushy/API/public/index.php/v1" // You won't change that
											 andAppSecret:@"bw4gkhuilw4gc3lnwt8bcw458bzw3ojt45zbuwo845ntlo8"	// Set the target's App-Secret here
											  andTargetID:@"2"													// Set your target's ID here
									andReadableIdentifier:@"WP2"];												// You need to set something here to identify the source/target later, like "MyWordPress-1"
	

    // Add some notification-observers to count running network-requests. This gives us the opportunity to start/stop the NetworkIndicator to indicate that we're doing stuff
    // You should do this, too. But you might do this in a way that fits your application better. But this is for demo-reasons so you know which notifications you should observe for this.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(incrementNumberOfRunningNetworkRequestsByNotification:) name:BIPUSHY_DID_START_NETWORK_REQUEST_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(decrementNumberOfRunningNetworkRequestsByNotification:) name:BIPUSHY_DID_FINISH_NETWORK_REQUEST_NOTIFICATION object:nil];
	
	self.pushyLibrary.launchOptions = launchOptions; // Pass the launchOptions to handle App-Launch by Push-Notification
	
    // Register for Push-Notitifcations, move that to a point of your choice.
	
    // Get your categories by the helper method
    NSSet *categories = [self getDefaultActionableNotifications];
	
	// We decide to regsister both targets for push
	// Let the user select what he wants to subscribe to
	[self.pushyLibrary doActionForTargets:@[target1, target2] andAction:^(BITarget *target)
	{
		// Pass the user's preferences about alerts/bages/sounds and, if given, actionable-categories.
		[target registerForNotificationsWithAlert:YES andBadge:YES andSound:YES andCategories:categories];

	}];
	
	
    return YES;
}




#pragma mark - Required APNS-Interaction Methods

// Alwas handle errors on registration!
- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err
{
    // You should handle this error!
    NSLog(@"Error in APNS registration. Error: %@", err);
}

// Called if the register-process was successfull and we've got a DeviceToken
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // Always tell the Pushy-Library about the DeviceToken
    // Pushy-Library will take care about server-registration etc.
    [self.pushyLibrary didRegisterForRemoteNotificationsWithDeviceToken:deviceToken withCallback:^()
     {
		 // We need to refer to a specific target for the further actions
		 BITarget *WP1 = [self.pushyLibrary getTargetForReadableIdentifier:@"WP1" andTargetIdentifier:@"1"];
		 
         // You've to wait for Meta-Data, it becomes available in this callback. It's not any earlier available.
         // So, don't start with Meta-Operations until this callback!
         
         // Here you can see an example for meta usage
         NSString *deviceName = [[UIDevice currentDevice] name];
         
         // If the remote DeviceName needs to be updated…
         if([[WP1 getMetaValueForKey:@"device_name"] isEqualToString:@""] || ![[WP1 getMetaValueForKey:@"device_name"] isEqualToString:deviceName])
         {
             // … update the DeviceName
             [WP1 setMetaValue:deviceName forKey:@"device_name"];
         }
         
         // See what we've got now
         NSLog(@"Device Name: %@", [WP1 getMetaValueForKey:@"device_name"]);
         
         
         // Repeat this with the DeviceOS, just as another example
         NSString *deviceOS = [[UIDevice currentDevice] systemVersion];
         
         if([[WP1 getMetaValueForKey:@"device_os"] isEqualToString:@""] || ![[WP1 getMetaValueForKey:@"device_os"] isEqualToString:deviceName])
         {
             // … update the DeviceOS
             [WP1 setMetaValue:deviceOS forKey:@"device_os"];
         }
         
         // See what we've got now
         NSLog(@"Device OS: %@", [WP1 getMetaValueForKey:@"device_os"]);
         
         
         // Now an example that shows the basic-usage of the UserID
         // You should put this whole "set UserID code" somewhere else.
         // Eg. after a login for comments or anything that works similar.
         // You might use your own Login-System to get the unique ID
         // Otherwise: You could just hash the e-mail address he provided for comments
         // Note: This works only with integers
         
         // Check whether we've got already an UserID
         if([WP1 getUserID] == 0)
         {
             // Our User wasn't logged in, so he had no UserID.
             // But now, he's logged in and we set his UserID.
             [WP1 setUserID:42];
         }
         
     }];

}

// Called if we received a RemoteNotification and the application is currently in foreground
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    // In further releases this behavior might be put in application:didReceiveRemoteNotification:fetchCompletionHandler: to enable pre-downloads of pushed-articles
	
    // Always tell the Pushy-Library about the notification and its' content
    // So the Pushy-Library can post notifications internally
    [self.pushyLibrary didReceiveRemoteNotification:userInfo];
}

// This method is called for actionable-notification interaction (except quick-reply)
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler
{

    // Always tell the Pushy-Library about the notification and its' content
    // So the Pushy-Library can post notifications internally
    // We are able to get the parsed Notification-Object directly
    NSDictionary *parsedNotifcation = [self.pushyLibrary didReceiveRemoteNotification:userInfo];
	
	// We need to check whether the target mapping worked!
	if(parsedNotifcation != nil)
	{
		// You always need to refer to a specific target
		// We can retrieve it from the notification itself and automatically do the action for the right target
		BITarget *currentTarget = [self.pushyLibrary getTargetForReadableIdentifier:[parsedNotifcation objectForKey:BIPUSHY_PUSH_DICT_READABLE_ID] andTargetIdentifier:[parsedNotifcation objectForKey:BIPUSHY_PUSH_DICT_TARGET_ID]];
		
		if ([parsedNotifcation[BIPUSHY_PUSH_DICT_PUSH_CATEGORY] isEqualToString:NotificationPostCategoryIdentifier])
		{
			if([identifier isEqualToString:NotificationPostActionReadIdentifier])
			{
				// You might not reach this point (in time), because the call in line 70 fires local notifications, so your App is doing things like it's implemented for App-launch by a Push-Notification
			}
			else if ([identifier isEqualToString:NotificationPostActionMarkReadIdentifier])
			{
				// Here you should put all the code that's required to just mark the article as read. Keep in mind, you've just seconds to do this, because it's running in Background
				
				// At first, we need to tell the library, that we're running minimal, because we're on background-mode
				[[BIPushy sharedInstance] setAppRunningMode:BIAppRunningModeMinimal];
				
				// Take sharedInstance of our AppDelegate's Pushy-Library and mark the current notification associated to the current article as read.
				// You don't need to track yourself whether the NF was previously marked as read, the server does this anyway.
				// This also lowers the Badge-Count automatically for you. Also it cancels all local notifications (clears NF-Center).
				[currentTarget markNotificationRead:[parsedNotifcation objectForKey:BIPUSHY_PUSH_DICT_NOTIFICATION_ID]];
			}
		}
		else if ([parsedNotifcation[BIPUSHY_PUSH_DICT_PUSH_CATEGORY] isEqualToString:NotificationCommentCategoryIdentifier])
		{
			if([identifier isEqualToString:NotificationCommentActionReplyIdentifier])
			{
				// The user wants to reply to the given comment, this is a foreground action!
				// So open the article and show the comments, jump directly into the TextField!
				// This is your task
			}
			else if ([identifier isEqualToString:NotificationCommentActionMuteIdentifier])
			{
				// The user does not want any Pushs for this article's comments anylonger.
				
				// Here you should put all the code that's required to mute the comments of the related post. Keep in mind, you've just seconds to do this, because it's running in Background
				
				// At first, we need to tell the library, that we're running minimal, because we're on background-mode
				[[BIPushy sharedInstance] setAppRunningMode:BIAppRunningModeMinimal];
				
				// Now, disable the comment push of the article
				NSString *metaKey = [NSString stringWithFormat:@"subscribes_comment_%@", [parsedNotifcation objectForKey:BIPUSHY_PUSH_DICT_POST_ID]];
				[currentTarget setMetaValue:[NSNumber numberWithBool:NO] forKey:metaKey];
				
				// You might want to mark this Notification as read
				[currentTarget markNotificationRead:[parsedNotifcation objectForKey:BIPUSHY_PUSH_DICT_NOTIFICATION_ID]];
			}
			
		}
		
	}
	
	
    // Do not forget about the CompletionHandler!
    if (completionHandler)
        completionHandler();
	
}

// This method is called if the user returned text from a quick-reply remote-notification (ATTENTION: AVAILABLE SINCE IOS9!!)
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo withResponseInfo:(NSDictionary *)responseInfo completionHandler:(void (^)())completionHandler
{
	// You always need to refer to a specific target
	BITarget *WP1 = [self.pushyLibrary getTargetForReadableIdentifier:@"WP1" andTargetIdentifier:@"1"];
	
    // Always tell the Pushy-Library about the notification and its' content
    // So the Pushy-Library can post notifications internally
    // We are able to get the parsed Notification-Object directly
    NSDictionary *parsedNotifcation = [WP1 didReceiveRemoteNotification:userInfo];
    
    if ([parsedNotifcation[BIPUSHY_PUSH_DICT_PUSH_CATEGORY] isEqualToString:NotificationCommentCategoryIdentifier])
    {
        if([identifier isEqualToString:NotificationCommentActionReplyIdentifier])
        {
            // At first, we need to tell the library, that we're running minimal, because we're on background-mode
            [[BIPushy sharedInstance] setAppRunningMode:BIAppRunningModeMinimal];
            
            // Just for the stats
            [WP1 markNotificationRead:[parsedNotifcation objectForKey:BIPUSHY_PUSH_DICT_NOTIFICATION_ID]];
            
            NSString *comment = responseInfo[UIUserNotificationActionResponseTypedTextKey];
            
            if (comment.length > 0)
            {
                // Your Server-Interaction-Code should be placed here
                
                // Just to demonstrate and "debug", you should remove this before submitting your application
                UILocalNotification *localNotification = [[UILocalNotification alloc] init];
                localNotification.alertTitle = @"Quick-Reply Demo";
                localNotification.alertBody = comment;
                localNotification.fireDate = [NSDate dateWithTimeInterval:5 sinceDate:[NSDate date]];
                [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
            }
            
        }
        
    }
    
    
    // Do not forget about the CompletionHandler!
    if (completionHandler)
        completionHandler();
    
}


#pragma mark - Utility Methods

// This is just a helper method to set up your actionable Notifications
- (NSSet *)getDefaultActionableNotifications
{
    // Normal Article Push
    
    
    // Define ReadAction
    UIMutableUserNotificationAction *readAction = [[UIMutableUserNotificationAction alloc] init];
    [readAction setActivationMode:UIUserNotificationActivationModeForeground]; // To read the Article, we need to launch the app in foreground (makes sense, yeha?).
    [readAction setTitle:@"Read"]; // You might want to localize this
    [readAction setIdentifier:NotificationPostActionReadIdentifier]; // Pass our identifier
    //[readAction setDestructive:NO]; // We don't want this button red
    [readAction setAuthenticationRequired:YES]; // Because we actualy need to launch the app in foreground
    
    // Define MarkAsReadAction
    UIMutableUserNotificationAction *markReadAction = [[UIMutableUserNotificationAction alloc] init];
    [markReadAction setActivationMode:UIUserNotificationActivationModeBackground]; // We can do this in BackgroundMode.
    [markReadAction setTitle:@"Mark Read"]; // You might want to localize this
    [markReadAction setIdentifier:NotificationPostActionMarkReadIdentifier]; // Pass our identifier
    [markReadAction setDestructive:YES]; // Do we want this button red? Decide yourself.
    [markReadAction setAuthenticationRequired:NO]; // Because we just mark it internally and remote as read, if you need FileAccess or so, you might want to change this
    
    // Put the actions into a category
    UIMutableUserNotificationCategory *artcileActionCategory = [[UIMutableUserNotificationCategory alloc] init];
    [artcileActionCategory setIdentifier:NotificationPostCategoryIdentifier];
    [artcileActionCategory setActions:@[readAction, markReadAction] forContext:UIUserNotificationActionContextDefault];
    
    
    // Comment Push
    UIMutableUserNotificationAction *replyAction = [[UIMutableUserNotificationAction alloc] init];
    [replyAction setTitle:@"Reply"]; // You might want to localize this
    [replyAction setIdentifier:NotificationCommentActionReplyIdentifier]; // Pass our identifier
    [replyAction setDestructive:NO]; // We don't want this button red
    
    // Check whether Quick-Reply is available (ATTENTION: AVAILABLE SINCE IOS9)
    if([replyAction respondsToSelector:@selector(setBehavior:)])
    {
        /*
         * If you actually want to make Quick-Reply a feature: read carefully.
         * Yes, Quick-Reply is a time-safer and makes things a lot easier for users.
         * BUT we've got a problem. Quick-Reply does only make sense if a user can
         * read the most part of the content he wants to respond to. Compare this to
         * iMessage, you can't give a real response if you're only able to read the
         * first seven words of the message. So the user needs to be able to read the
         * biggest part of the message. Apple automatically truncates the part of the
         * message that can not be viewed on the device, that's pretty helpful.
         * To come to the real problem: Whether this is a problem to you, depends on
         * your deployment target. Since Apple decided to support different sizes of
         * payloads depending on the OS, we're in trouble:
         *
         * iOS9: 4KB (currently undocumented)
         * iOS8: 2KB (this value is taken from APNs documentation)
         * iOS7 and prior: 256B
         *
         * What does this mean? This means you've to know your target-audience:
         * If you're only supporting iOS9+, you can go up to 4096 Bytes.
         * If you're only supporting iOS8+, you can go up to 2048 Bytes.
         * If you're supporting iOS7+, you can not go higher than 256 Bytes.
         *
         * Depending on your custom-payload-configuration, 2048 Bytes will work
         * for a nice Quick-Reply experience. 256 Bytes mean, you should not use
         * Quick-Reply or drop iOS7 support.
         *
         * BIPushy REST-API can only deal with one setting for all. So it's a problem
         * and it's currently nearly impossible to implement this depending on the
         * device OS. There might be ways, but at this time I'm not going to support this.
         *
         * If you've chosen your target-audience 256/2048/4096 Bytes, you can change the
         * value of AVAILABLE_PAYLOAD_BYTES in line 23 wp-content/plugins/BIPushy/API/config.php
         * Default value is 256 Bytes. I'm maybe going to put this in settings, so it will not
         * be overwritten during updates.
         */
        
        [replyAction setActivationMode:UIUserNotificationActivationModeBackground]; // We can work in Background
        [replyAction setBehavior:UIUserNotificationActionBehaviorTextInput]; // We want Quick-Reply!
        [replyAction setAuthenticationRequired:NO];  // You might want to change this
    }
    else
    {
        [replyAction setActivationMode:UIUserNotificationActivationModeForeground]; // To show the Comments, we need to launch the app in foreground (makes sense, yeha?).
        [replyAction setAuthenticationRequired:YES]; // Because we actualy need to launch the app in foreground
    }

    
    // Define Mute Thread Action
    UIMutableUserNotificationAction *muteThreadCommentsAction = [[UIMutableUserNotificationAction alloc] init];
    [muteThreadCommentsAction setActivationMode:UIUserNotificationActivationModeBackground]; // We can do this in BackgroundMode.
    [muteThreadCommentsAction setTitle:@"Mute"]; // You might want to localize this
    [muteThreadCommentsAction setIdentifier:NotificationCommentActionMuteIdentifier]; // Pass our identifier
    [muteThreadCommentsAction setDestructive:YES]; // Do we want this button red? Decide yourself.
    [muteThreadCommentsAction setAuthenticationRequired:NO]; // Because we just mark it internally and remote as read, if you need FileAccess or so, you might want to change this
    
    
    // Put the actions into a category
    UIMutableUserNotificationCategory *commentActionCategory = [[UIMutableUserNotificationCategory alloc] init];
    [commentActionCategory setIdentifier:NotificationCommentCategoryIdentifier];
    [commentActionCategory setActions:@[muteThreadCommentsAction, replyAction] forContext:UIUserNotificationActionContextDefault];
    
    
    
    // Return it as a NSSet
    return [NSSet setWithObjects:artcileActionCategory, commentActionCategory, nil];
}


#pragma mark - Network Request Counting Methods

// You might have this functionality implemented already your way

// Called if a new network request was just started
- (void)incrementNumberOfRunningNetworkRequestsByNotification:(NSNotification *)nf
{
    // Increment the number of currently running network requests
    numberOfRunningNetworkRequests++;
    
    // Tell the NetworkInidcator abou the update
    [self updateNetworkIndicator];
}

// Called if a network request ended (successfully or by failure)
- (void)decrementNumberOfRunningNetworkRequestsByNotification:(NSNotification *)nf
{
    // Decrement the number of currently running network requests
    numberOfRunningNetworkRequests--;
    
    // Tell the NetworkInidcator abou the update
    [self updateNetworkIndicator];
}

// Called if the number of running network requests has changed
- (void)updateNetworkIndicator
{
    // If there was no running request, this is a new one so start spinning!
    if(numberOfRunningNetworkRequests > 0 && !isNetworkIndicatorRunning)
    {
        // Store this to not re-start the spinner on every request (that'd be pretty ugly)
        isNetworkIndicatorRunning = YES;
        
        // Tell the network activity indicator to spin
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    }
    else if (numberOfRunningNetworkRequests == 0) // If we finished all network requests, we should stop the spinner
    {
        // Tell the network activity indicator to stop
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        // Store this to not re-start the spinner on every request (that'd be pretty ugly)
        isNetworkIndicatorRunning = NO;
    }
    
}

@end
