//
//  BIPushDetailViewController.m
//  PushyClient
//
//  Created by Jan Galler on 18/02/15.
//  Copyright (c) 2015 byte.it [Kees Galler] GbR. All rights reserved.
//

#import "BIPushDetailViewController.h"

@implementation BIPushDetailViewController


#pragma mark - Initialization Methods

- (void)setUpObservers
{
    // Wait for the kind of notifications we want to inform the user about. You might not want to listen to all notifications.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showNewNotification:) name:BIPUSHY_DID_RECEIVE_ALL_NOTIFICATION object:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    // Start observing for incoming notifications
    [self setUpObservers];
    
    
    // This is just some UI stuff for demo-reasons
    self.title = [self.pushData objectForKey:BIPUSHY_PUSH_DICT_POST_ID];
    self.pushTitle.text = [self.pushData objectForKey:BIPUSHY_PUSH_DICT_TITLE];
	//self.pushAlert.text = [self.pushData objectForKey:@"Alert"];
	self.pushAlert.text = [NSString stringWithFormat:@"%@ (From Readable-Identifier: %@)",[self.pushData objectForKey:BIPUSHY_PUSH_DICT_ALERT], [self.pushData objectForKey:BIPUSHY_PUSH_DICT_READABLE_ID]];
	
    
    // Check whether subscription for comments of this post are enabled
    NSString *metaKey = [NSString stringWithFormat:@"subscribes_comment_%@", [self.pushData objectForKey:BIPUSHY_PUSH_DICT_POST_ID]];
    NSInteger metaValue = [[[[BIPushy sharedInstance] getTargetForReadableIdentifier:[self.pushData objectForKey:BIPUSHY_PUSH_DICT_READABLE_ID] andTargetIdentifier:[self.pushData objectForKey:BIPUSHY_PUSH_DICT_TARGET_ID] ] getMetaValueForKey:metaKey] integerValue];
    [self.subscribeCommentsSwitch setOn:metaValue animated:YES];
    
    
    // After the article is visible to the user and he's able to read, you've to decide "when" a user has read a NF
    // For demo-reasons we define that after 1 second the user has read the NF
    // You might want to implement this in other ways and at other points. It's fully up to you, but keep in mind that this influences the stats.
    // Also take care of "manual" NF reads, which are not associated to articles!
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^
    {
        // Take sharedInstance of our AppDelegate's Pushy-Library and mark the current notification associated to the current article as read.
        // You don't need to track yourself whether the NF was previously marked as read, the server does this anyway.
        // This also lowers the Badge-Count automatically for you. Also it cancels all local notifications (clears NF-Center).
        [[[BIPushy sharedInstance] getTargetForReadableIdentifier:[self.pushData objectForKey:BIPUSHY_PUSH_DICT_READABLE_ID] andTargetIdentifier:[self.pushData objectForKey:BIPUSHY_PUSH_DICT_TARGET_ID]] markNotificationRead:[self.pushData objectForKey:BIPUSHY_PUSH_DICT_NOTIFICATION_ID]];
    });
    
}


#pragma mark - UI Interaction Methods

- (IBAction)pushSubscribeCommentsSwitch:(id)sender
{
    // Build the MetaKey by the scheme: subscribes_comment_x, x is the PostID
    NSString *metaKey = [NSString stringWithFormat:@"subscribes_comment_%@", [self.pushData objectForKey:BIPUSHY_PUSH_DICT_POST_ID]];
    // Tell the library about the user's preferences
    [[[BIPushy sharedInstance] getTargetForReadableIdentifier:[self.pushData objectForKey:BIPUSHY_PUSH_DICT_READABLE_ID] andTargetIdentifier:[self.pushData objectForKey:BIPUSHY_PUSH_DICT_TARGET_ID]] setMetaValue:[NSNumber numberWithBool:[self.subscribeCommentsSwitch isOn]] forKey:metaKey];
}

// Just UI Method to inform the user about new articles. We teaser new articles through the StatusBar
// You should do this in a way it fits your application. It's important to inform the user about new Articles without interrupt his reading
- (void)showNewNotification:(NSNotification *)nf
{
    [JDStatusBarNotification showWithStatus:[NSString stringWithFormat:@"%@ %@",[[nf userInfo] objectForKey:@"Title"], [[nf userInfo] objectForKey:BIPUSHY_PUSH_DICT_ALERT]] dismissAfter:2.0 styleName:JDStatusBarStyleSuccess];
}



@end
