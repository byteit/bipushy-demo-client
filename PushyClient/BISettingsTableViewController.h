//
//  BISettingsTableViewController.h
//  PushyClient
//
//  Created by Jan Galler on 19/08/15.
//  Copyright (c) 2015 byte.it [Kees Galler] GbR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BIPushy.h"

@interface BISettingsTableViewController : UITableViewController <UITextFieldDelegate>

@property (strong, nonatomic) BITarget *WP1;

@property (weak, nonatomic) IBOutlet UITextField *userIDTextField;
@property (weak, nonatomic) IBOutlet UISwitch *subscribeCommentsSwitch;

- (IBAction)pushDone:(id)sender;
- (IBAction)pushClearBadgeButton:(id)sender;
- (IBAction)pushSubscribeCommentsSwitch:(id)sender;

@end
