//
//  BITarget.h
//  BIPushy
//
//  Created by Jan Galler on 30/10/16.
//  Copyright © 2016 byte.it [Kees Galler] GbR. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface BITarget : NSObject

@property (strong, nonatomic, readonly) NSString *deviceToken;
@property (strong, nonatomic) NSDictionary *launchOptions;

@property (strong, nonatomic) NSString *targetID;
@property (strong, nonatomic) NSString *baseURL;
@property (strong, nonatomic) NSString *APIEndPoint;
@property (strong, nonatomic) NSString *appSecret;
@property (strong, nonatomic) NSString *readableIdentifier;

- (instancetype)initWithBaseURL:(NSString *)baseURL
				 andAPIEndPoint:(NSString *)APIEndPoint
				   andAppSecret:(NSString *)appSecret
					andTargetID:(NSString *)targetID
		  andReadableIdentifier:(NSString *)readableIdentifier;

- (void)registerForNotificationsWithAlert:(BOOL)useAlerts andBadge:(BOOL)useBadge andSound:(BOOL)useSound;
- (void)registerForNotificationsWithAlert:(BOOL)useAlerts andBadge:(BOOL)useBadge andSound:(BOOL)useSound andCategories:(NSSet *)categories;

- (void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken withCallback:(void (^)())callback;
- (NSDictionary *)didReceiveRemoteNotification:(NSDictionary *)notification;

- (void)lowerBadgeCount;
- (void)increaseBadgeCount;
- (void)setBadgeCount:(NSUInteger)count;

- (void)markNotificationRead:(NSString *)notificationID;

- (NSDictionary *)getMetaDictionary;
- (id)getMetaValueForKey:(NSString *)key;
- (void)setMetaValue:(id)value forKey:(NSString *)key;
- (NSInteger)getUserID;
- (void)setUserID:(NSInteger)userID;


@end
