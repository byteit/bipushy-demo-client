//
//  Pushy.h
//  Pushy
//
//  Created by Jan Galler on 18/02/15.
//  Copyright (c) 2015 byte.it [Kees Galler] GbR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BITarget.h"


//! Project version number for Pushy.
FOUNDATION_EXPORT double PushyVersionNumber;

//! Project version string for Pushy.
FOUNDATION_EXPORT const unsigned char PushyVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Pushy/PublicHeader.h>



// Notification Strings
#define BIPUSHY_DID_RECEIVE_ALL_NOTIFICATION                @"BIPushyDidReceiveAllNotification"
#define BIPUSHY_DID_RECEIVE_NEW_ARTICLE_NOTIFICATION        @"BIPushyDidReceiveNewArticleNotification"
#define BIPUSHY_DID_RECEIVE_UPDATED_ARTICLE_NOTIFICATION    @"BIPushyDidReceiveUpdatedArticleNotification"
#define BIPUSHY_DID_RECEIVE_MANUAL_NOTIFICATION             @"BIPushyDidReceiveManualNotification"
#define BIPUSHY_DID_RECEIVE_NEW_COMMENT_NOTIFICATION        @"BIPushyDidReceiveNewCommentNotification"
#define BIPUSHY_DID_START_NETWORK_REQUEST_NOTIFICATION      @"BIPushyDidStartNetworkRequestNotification"
#define BIPUSHY_DID_FINISH_NETWORK_REQUEST_NOTIFICATION     @"BIPushyDidFinishNetworkRequestNotification"

// Push-Dict Content-Keys
#define BIPUSHY_PUSH_DICT_READABLE_ID       @"BIPushyPushDictReadableIdentifier"
#define BIPUSHY_PUSH_DICT_TARGET_ID         @"BIPushyPushDictTargetIdentifier"
#define BIPUSHY_PUSH_DICT_NOTIFICATION_ID   @"BIPushyPushDictNotificationIdentifier"
#define BIPUSHY_PUSH_DICT_POST_ID           @"BIPushyPushDictPostIdentifier"
#define BIPUSHY_PUSH_DICT_COMMENT_ID        @"BIPushyPushDictCommentIdentifier"
#define BIPUSHY_PUSH_DICT_RAW_DICT          @"BIPushyPushDictRawDict"
#define BIPUSHY_PUSH_DICT_ALERT             @"BIPushyPushDictAlertDict"
#define BIPUSHY_PUSH_DICT_IS_LAUNCH_PUSH    @"BIPushyPushDictIsLaunchPush"
#define BIPUSHY_PUSH_DICT_PUSH_CATEGORY     @"BIPushyPushDictPushCategory"
#define BIPUSHY_PUSH_DICT_TITLE             @"BIPushyPushDictTitle"
#define BIPUSHY_PUSH_DICT_IMAGE_URL         @"BIPushyPushDictImageURL"

// AppRunning-Mode
typedef NS_ENUM(NSInteger, BIAppRunningMode) {
	BIAppRunningModeDefault,
	BIAppRunningModeMinimal,
};


// Main Interface-Class
@interface BIPushy : NSObject

@property () BIAppRunningMode appRunningMode;
@property (strong, nonatomic) NSDictionary *launchOptions;
@property () BOOL cancelAllNotificationsOnMarkRead;

@property (strong, nonatomic) NSMutableDictionary *targets;

+ (instancetype)sharedInstance;

- (void)registerAllTargetsForNotificationsWithAlert:(BOOL)useAlerts andBadge:(BOOL)useBadge andSound:(BOOL)useSound;
- (void)registerAllTargetsForNotificationsWithAlert:(BOOL)useAlerts andBadge:(BOOL)useBadge andSound:(BOOL)useSound andCategories:(NSSet *)categories;

- (NSMutableDictionary *)getAllTargets;
- (BITarget *)getTargetForReadableIdentifier:(NSString *)readableIdentifier andTargetIdentifier:(NSString *)targetIdentifier;
- (NSArray *)getReadableIdentifiers;
- (void)doActionForAllTargets:(void (^)(BITarget *target))actionCallback;
- (void)doActionForTargets:(NSArray *)targets andAction:(void (^)(BITarget *target))actionCallback;


- (void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken withCallback:(void (^)())callback;
- (NSDictionary *)didReceiveRemoteNotification:(NSDictionary *)notification;

- (void)lowerBadgeCountForAllTargets;
- (void)increaseBadgeCountForAllTargets;
- (void)setBadgeCountForAllTargets:(NSUInteger)count;


@end
