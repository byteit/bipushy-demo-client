//
//  BIPushNotificationsTableViewController.h
//  PushyClient
//
//  Created by Jan Galler on 18/02/15.
//  Copyright (c) 2015 byte.it [Kees Galler] GbR. All rights reserved.
//

#import <UIKit/UIKit.h>
// We need this header here for NF-handling, so import it!
#import "BIPushy.h"
// We also need our custom DetailViewController to pass data on row-tapping
// You might already have a DetailViewController, so you wont need this
#import "BIPushDetailViewController.h"

@interface BIPushNotificationsTableViewController : UITableViewController


@end
