//
//  AppDelegate.h
//  PushyClient
//
//  Created by Jan Galler on 13/02/15.
//  Copyright (c) 2015 byte.it [Kees Galler] GbR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BIPushy.h"
#import "BITarget.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    // Attributes to handle NetworkIndicator Status, optional but you should implement such kind of functionality
    int numberOfRunningNetworkRequests;
    BOOL isNetworkIndicatorRunning;
}

// This is what you need in your application for Pushy
@property (strong, nonatomic) BIPushy *pushyLibrary;

// Just fpr UI-reasons
@property (strong, nonatomic) UIWindow *window;


@end

