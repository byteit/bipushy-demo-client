//
//  BIPushDetailViewController.h
//  PushyClient
//
//  Created by Jan Galler on 18/02/15.
//  Copyright (c) 2015 byte.it [Kees Galler] GbR. All rights reserved.
//

#import <UIKit/UIKit.h>
// We need this header here for NF-reads, so import it
#import "BIPushy.h"
// This is just an UI-thing, you don't have to do this in your application
#import "JDStatusBarNotification.h"

@interface BIPushDetailViewController : UIViewController

// This attribute is very important, we here store our NF-data to pass the NotificationID for NF-read.
// This has to be public, so our MainViewController is able to set this attribute. Or write a setter yourself.
// You might want to add proper memory-statements that fit your application
// You also might want to just include the NotificationID of the Dictionairy to your own DataStructure.
@property NSDictionary *pushData;


// Just UI attributes

@property (weak, nonatomic) IBOutlet UILabel *pushTitle;
@property (weak, nonatomic) IBOutlet UILabel *pushAlert;
@property (weak, nonatomic) IBOutlet UISwitch *subscribeCommentsSwitch;

// Our UI Action Methods
- (IBAction)pushSubscribeCommentsSwitch:(id)sender;

@end
