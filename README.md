# README #

This is an example implementation of the BIPushy-iOS-Library. This Demo-App should give you a quick overview about the features of the library and how you can implement them.


## Features ##
* Handles APNs-Registration 
* Does all plugin/server communication you need
* Article and Comment Push
* Takes care of the Badge-Counter
* Read receipts for Push-Notifications
* Actionable Push-Notifications (also Quick-Reply)
* Storing/Retrieving Meta-Data per Device-Token
* UserID per Device-Token
* Filters and passes remote Push-Notifications as `NSNotification`, different types of remote Push-Notifications will cause different `NSNotification` to be sent
 * Support for multiple targets (meaning multiple BIPushy-Server instances)


## What this is not ##
* Sample implementation of a WordPress Article-Reader
* Sample implementation of every available feature, it's just a basic example
* Sample implementation that fits everyone's need, you might have to implement all the things on your own, depending on your setup


## Dependencies ##

* You need a WordPress 4.0 (or greater) installation
* Your WordPress needs the configured BIPushy-Plugin (with iOS extension)
    * A configured and enabled iOS-Target
    * The App-Secret from the iOS-Target
* An Apple-Developer-Account with a ProvisioningProfile that supports APNs
* The BIPushy-iOS-Library (libBIPushy.a)
* Your iOS-Device must run iOS 8.0 (or greater), the libBIPushy.a is universally compiled (32/64bit Simulator/Device)


## Structure and behaviour ##

The library requires you to initialize so called targets. Targets are basically API end-points where a BIPushy-Server instance is running.
So you can have multiple targets which means multiple sources that trigger push notifications. To differenciate between the sources and to handle registration and further stuff, it is required to register the targets. Usually, you will just have a single target and this should be fine. But for advanced purpouses, the library supports multiple targets. Some features might not work as expected, so be aware of the possible problems with mutliple targets (eg. the badge-counter will be wrong, so use the 'one-mode' only).

Having multiple targets requires you also to handle everything per target, BIPushy-methods try to make this as easy as possible without redundant code. But you have to do everything on a target-base. 

The fact that you can have multiple targets requires you also to define identifiers for targets. If you have two BIPushy-Server instances running, the library has to be able to differenciate between these. Thus, it is required to embed a unique-identifier into every push-notification sent be the two instances. Otherwise the library can not tell the targets apart. These identifiers are called 'readable identifiers' because they should be something you read and remeber in your code. Because to retrieve a prior registered target from BIPushy, you have to pass the readable identifier. These will also be included in the `NSNotification` fired by the BIPushy libarary.

Thus you have to handle registration an a per target-base, you should allow the user to choose the targets on his preferences. Do not force the user to subsribe to all targets, let him decide.

The library offers you a general entrance by using `BIPushy` (see section below) and a specific, but required, interaction by the use of`BITarget`.


## Available public Methods of BIPushy ##

Use this method to create an instance of BIPushy, do not try to initialize it on your own!

* `+ (instancetype)sharedInstance;`

Methods for general setup:

* `- (void)registerAllTargetsForNotificationsWithAlert:(BOOL)useAlerts andBadge:(BOOL)useBadge andSound:(BOOL)useSound;`
* `- (void)registerAllTargetsForNotificationsWithAlert:(BOOL)useAlerts andBadge:(BOOL)useBadge andSound:(BOOL)useSound andCategories:(NSSet *)categories;`
* `- (void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken withCallback:(void (^)())callback`
* `- (NSDictionary *)didReceiveRemoteNotification:(NSDictionary *)notification;`


Methods to manage the App's badge-counter:

* `- (void)lowerBadgeCountForAllTargets;`
* `- (void)increaseBadgeCountForAllTargets;`
* `- (void)setBadgeCountForAllTargets:(NSUInteger)count;`

Methods to manage targets (`BITarget` instances):

* `- (NSMutableDictionary *)getAllTargets;` Use this method to retrieve all registered targets
* `- (BITarget *)getTargetForReadableIdentifier:(NSString *)readableIdentifier;` Use this method to retrieve a specific target, identified by its readable-identifier
* `- (NSArray *)getReadableIdentifiers;` Returns a list of all registered readable-identifiers
* `- (void)doActionForAllTargets:(void (^)(BITarget *target))actionCallback;` Use this method to distribute data to all targets or do an action for all targets. This allows you to write non-redundant code.
* `- (void)doActionForTargets:(NSArray *)readableIdentifiers andAction:(void (^)(BITarget *target))actionCallback` Use this method to distribute data to specified targets or do an action for specified targets. This allows you to write non-redundant code.



## Available public Methods of BITarget ##


Use this method to create an instance of BITarget, do not try to initialize it otherwise.

* `- (instancetype)initWithBaseURL:(NSString *)baseURL
				 andAPIEndPoint:(NSString *)APIEndPoint
				   andAppSecret:(NSString *)appSecret
					andTargetID:(NSString *)targetID
		  andReadableIdentifier:(NSString *)readableIdentifier;` Use this method to specify the BIPushy-Server instance of the new target. Choose a good and unique `readableIdentifier` on your own. `targetID` means the int that is given to the target in the database of the BIPushy-Server instance.

_NOTE_: Methods listed below might be proxied by using `BIPushy` to automatically do certain methods for all/specific targets.

Methods for general setup:

* `- (void)registerForNotificationsWithAlert:(BOOL)useAlerts andBadge:(BOOL)useBadge andSound:(BOOL)useSound;`
* `- (void)registerForNotificationsWithAlert:(BOOL)useAlerts andBadge:(BOOL)useBadge andSound:(BOOL)useSound andCategories:(NSSet *)categories;`
* `- (void)unregisterForRemoteNotificationsWithCallback:(void (^)())callback;`
* `- (void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken withCallback:(void (^)())callback`
* `- (NSDictionary *)didReceiveRemoteNotification:(NSDictionary *)notification;`

Methods to manage the App's badge-counter:

* `- (void)lowerBadgeCount;`
* `- (void)increaseBadgeCount;`
* `- (void)setBadgeCount:(NSUInteger)count;`

Methods related to read receipts:

* `- (void)markNotificationRead:(NSString *)notificationID;`

Methods to mange Meta-Data of a DeviceToken

* `- (NSDictionary *)getMetaDictionary;`
* `- (id)getMetaValueForKey:(NSString *)key;`
* `- (void)setMetaValue:(id)value forKey:(NSString *)key;`

Methods to mange the UserID of a DeviceToken

* `- (NSInteger)getUserID;`
* `- (void)setUserID:(NSInteger)userID;`

_NOTE:_ Methods that interact with the server do need the DeviceToken, but this might take some time to be available to the library. The library caches the DeviceToken for you, if you don't have time to wait for it (you have to specifiy that you want this). But in any other cases, your actions will be stored. The stored actions will be executed if the DeviceToken becomes available. If the DeviceToken is already available, your action will be executed as expected. The stored actions are executed by the order you stored them.



## Available public Properties of BIPushy ##

* `@property BIAppRunningMode appRunningMode;` With this property you can indicate whether the app is currently running in background and the DeviceToken might not be available. Eg. if you're working with actionable notifications in background-mode, you need to set this to `BIAppRunningModeMinimal`! In all other cases, where you have the time to wait for the DeviceToken, please use `BIAppRunningModeDefault`, because Apple does not want us to cache the DeviceToken. But we're using a cached DeviceToken in case of a minimal running App. So please don't use this, unless you're 100% sure what you're doing. This enum has the following values: `BIAppRunningModeDefault` and `BIAppRunningModeMinimal`.
* `@property () BOOL cancelAllNotificationsOnMarkRead;` This property let's you choose whether you want to automatically dismiss all notifications available on launch and mark-read-action. The default value is NO, because otherwise, in case of actionable notifications, you'd mark one notification read but all will be dismissed.  
* `@property (strong, nonatomic) NSMutableDictionary *targets;` You can directly access all registered targets, but you should not.


## Available public Properties of BITarget ##

* `@property (strong, nonatomic) NSString *readableIdentifier;` This is should be a unique identifier to tell multiple targets apart. This has to be in the payload of every push-notification respectivly. Choose something you can remeber and identify in your code. 
* `@property (strong, nonatomic) NSString *targetID;` The ID (int) of the target that is registered in the corresponding BIPushy-Server instance.  
* `@property (strong, nonatomic, readonly) NSString *deviceToken;` This property contains the DeviceToken's string-representation, if already available! 
* `@property (strong, nonatomic) NSString *baseURL;` With this property you can set your target-base URL, use `scheme://your-url.tld` without trailing slash!
* `@property (strong, nonatomic) NSString *APIEndPoint;` Here you have to set the plugin's API-EndPoint. You might want to copy this 1:1 from the example, otherwise have a look at the plugin's settings.
* `@property (strong, nonatomic) NSString *appSecret;` With this property you should set the right App-Secret-Token for your current target and mode.
* `@property (strong, nonatomic) NSDictionary *launchOptions;` Pass your application's launchOptions to BIPushy by setting this property. You should not really use this property, refer to the same property of `BIPushy` directly.


## Available Notifications ##

Push-Notification received Notifications:

* `BIPUSHY_DID_RECEIVE_ALL_NOTIFICATION` is sent whenever a Push-Notification was caught, the kind doesn't matter.
* `BIPUSHY_DID_RECEIVE_NEW_ARTICLE_NOTIFICATION` is sent when a Push-Notification for a new article was caught.
* `BIPUSHY_DID_RECEIVE_UPDATED_ARTICLE_NOTIFICATION` is sent when a Push-Notification for an update of an article was caught.
* `BIPUSHY_DID_RECEIVE_MANUAL_NOTIFICATION` is sent when a manual (custom-text) Push-Notification was caught.
* `BIPUSHY_DID_RECEIVE_NEW_COMMENT_NOTIFICATION` is sent when Push-Notification for a new comment was caught.

Notifications to monitor network activity:

* `BIPUSHY_DID_START_NETWORK_REQUEST_NOTIFICATION` is sent whenever a new network request starts.
* `BIPUSHY_DID_FINISH_NETWORK_REQUEST_NOTIFICATION` is sent whenever an ongoing network requets stops (failed or succeeded).

_NOTE:_ The `NSNotification` will, in case it was delivered by the payload, contain the readable-identifier of the target, so you can handle `NSNotification` different on a target-base.

## Usage ##

You can divide the whole functionality into ten main-steps:

1. Setup
2. APNS-Registration
3. Show Network-Activity
4. Catch incoming Notifications
    1. During Runtime
    2. Launch-by-Notification
5. Handle caught Notifications
6. Read receipt implementation for notifications
7. Actionable Notifications
8. Meta-Data Usage
9. UserID Usage
10. Comment-Push
    1. Subscription
    2. Global Subscription
    3. Interactive Comment-Push
    4. Comment-Push Quick-Reply

The first and second steps are required, but the others are optional. For best experience you should follow every step. The steps depend on each other in their order (e.g. step four requires step three).


### Step 1: Setup ###
* Copy libBIPushy.a (and headers) into your project
* Configure your project for static-libraries (you might not have to do anything)

### Step 2: APNS-Registration ###

##### YourAppDelegate.h #####
* Import `BIPushy.h`
* Create an instance-variable `@property (strong, nonatomic) BIPushy *pushyLibrary;`
##### YourAppDelegate.m #####
* Extend `- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions` as following:

``` objective-c
// Called on application-launch
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Initialize Pushy-Library on launch, very important!
    self.pushyLibrary = [BIPushy sharedInstance]; // Yep, this is a singleton. You might not like that, but it works.
    
	// Create a target, meaning here: a source of push notifications, a WordPress instance
	// You do not need to store this anywhere, the library makes it retrieveable by the readableIdentifier specified below
	BITarget *target1 = [[BITarget alloc] initWithBaseURL:@"http://my-wordpress-site-url.de"					// Your WordPress-Site's URL with http:// and without trailing-slash
																												// ATTENTION: Network requests might fail due App-Transport-Security if you do not use SSL.
																												// So create an exception to allow insecure calls to this URL, otherwise this whole thing will not work. Search-Keyword: NSAppTransportSecurity
																												// This will not be a problem if you use SSL. ATTENTION: Currently you can not use a different SSL-Port than 443.
										   andAPIEndPoint:@"wp-content/plugins/BIPushy/API/public/index.php/v1" // You won't change that
											 andAppSecret:@"my-very-secret-key"									// Set the target's App-Secret here
											  andTargetID:@"1"													// Set your target's ID here
									andReadableIdentifier:@"WP1"];												// You need to set something here to identify the source/target later, like "MyWordPress-1"
	
    // Let the user select what he wants to subscribe to. You need to register for every target separately!
	[self.pushyLibrary doActionForTargets:@[@"WP1"] andAction:^(BITarget *target)
	{
		// Pass the user's preferences about alerts/bages/sounds.
		[target registerForNotificationsWithAlert:YES andBadge:YES andSound:YES];

	}];


    return YES;
}
``` 

* Implement `- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error` and handle errors properly (you really should do this)
* Implement `- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken` as following:
``` objective-c
// Called if the register-process was successfull and we've got a DeviceToken
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // Always tell the Pushy-Library about the DeviceToken
    // Pushy-Library will take care about server-registration etc.
    [self.pushyLibrary didRegisterForRemoteNotificationsWithDeviceToken:deviceToken withCallback:^()
     {
         // You might want to do something useful here in future.
         // From this point DeviceToken, UserID and Meta-Data are available
     }];
}
```

### Step 3: Show Network-Activity ###
This and following steps are not required. This is a little feature you should take advantage of. It automatically shows and hides the Network-Activity-Indicator for you. You can do this also with your own Network-Requests (you should). Basically, this works like: The first person opens the door, several people go through and the last one closes the door.
This setp requires three little methods, two `NSNotification`-Observers and two instance variable for a counter and a flag.
##### YourAppDelegate.h #####
* Create the instance-variables for the counter and the flag:
``` objective-c
@interface YourAppDelegate : UIResponder <UIApplicationDelegate>
{
    // Attributes to handle NetworkIndicator Status, optional but you should implement such kind of functionality
    int numberOfRunningNetworkRequests;
    BOOL isNetworkIndicatorRunning;
}
```

##### YourAppDelegate.m #####
* Extend `- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions` as following:
``` objective-c
// Called on application-launch
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Initialize Pushy-Library on launch, very important!
    self.pushyLibrary = [BIPushy sharedInstance]; // Yep, this is a singleton. You might not like that, but it works.
    
	// Create a target, meaning here: a source of push notifications, a WordPress instance
	// You do not need to store this anywhere, the library makes it retrieveable by the readableIdentifier specified below
	BITarget *target1 = [[BITarget alloc] initWithBaseURL:@"http://my-wordpress-site-url.de"					// Your WordPress-Site's URL with http:// and without trailing-slash
																												// ATTENTION: Network requests might fail due App-Transport-Security if you do not use SSL.
																												// So create an exception to allow insecure calls to this URL, otherwise this whole thing will not work. Search-Keyword: NSAppTransportSecurity
																												// This will not be a problem if you use SSL. ATTENTION: Currently you can not use a different SSL-Port than 443.
										   andAPIEndPoint:@"wp-content/plugins/BIPushy/API/public/index.php/v1" // You won't change that
											 andAppSecret:@"my-very-secret-key"									// Set the target's App-Secret here
											  andTargetID:@"1"													// Set your target's ID here
									andReadableIdentifier:@"WP1"];												// You need to set something here to identify the source/target later, like "MyWordPress-1"
	
	
  	// Add some notification-observers to count running network-requests. This gives us the opportunity to start/stop the NetworkIndicator to indicate that we're doing stuff
    // You should do this, too. But you might do this in a way that fits your application better. But this is for demo-reasons so you know which notifications you should observe for this.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(incrementNumberOfRunningNetworkRequestsByNotification:) name:BIPUSHY_DID_START_NETWORK_REQUEST_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(decrementNumberOfRunningNetworkRequestsByNotification:) name:BIPUSHY_DID_FINISH_NETWORK_REQUEST_NOTIFICATION object:nil];

 
    // Let the user select what he wants to subscribe to. You need to register for every target separately!
	[self.pushyLibrary doActionForTargets:@[@"WP1"] andAction:^(BITarget *target)
	{
		// Pass the user's preferences about alerts/bages/sounds.
		[target registerForNotificationsWithAlert:YES andBadge:YES andSound:YES];

	}];


    return YES;
}
```
* Implement the mentioned methods as observer: `- (void)incrementNumberOfRunningNetworkRequestsByNotification:(NSNotification *)nf` and `- (void)decrementNumberOfRunningNetworkRequestsByNotification:(NSNotification *)nf` 
``` objective-c
#pragma mark - Network Request Counting Methods

// You might have this functionality implemented already your way

// Called if a new network request was just started
- (void)incrementNumberOfRunningNetworkRequestsByNotification:(NSNotification *)nf
{
    // Increment the number of currently running network requests
    numberOfRunningNetworkRequests++;
    
    // Tell the NetworkInidcator abou the update
    [self updateNetworkIndicator];
}

// Called if a network request ended (successfully or by failure)
- (void)decrementNumberOfRunningNetworkRequestsByNotification:(NSNotification *)nf
{
    // Decrement the number of currently running network requests
    numberOfRunningNetworkRequests--;
    
    // Tell the NetworkInidcator abou the update
    [self updateNetworkIndicator];
}
```
* Finally, we need to implement the `- (void)updateNetworkIndicator` method
``` objective-c
// Called if the number of running network requests has changed
- (void)updateNetworkIndicator
{
    // If there was no running request, this is a new one so start spinning!
    if(numberOfRunningNetworkRequests > 0 && !isNetworkIndicatorRunning)
    {
        // Store this to not re-start the spinner on every request (that'd be pretty ugly)
        isNetworkIndicatorRunning = YES;
        
        // Tell the network activity indicator to spin
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    }
    else if (numberOfRunningNetworkRequests == 0) // If we finished all network requests, we should stop the spinner
    {
        // Tell the network activity indicator to stop
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        // Store this to not re-start the spinner on every request (that'd be pretty ugly)
        isNetworkIndicatorRunning = NO;
    }
    
}
```


### Step 4: Catch incoming Notifications ###

### Step 4.1: During Runtime ###
##### YourAppDelegate.m #####
* Implement `- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo` as following:
``` objective-c
// Called if we received a RemoteNotification and the application is currently in foreground
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    // In further releases this behavior might be put in application:didReceiveRemoteNotification:fetchCompletionHandler: to enable pre-downloads of pushed-articles. You can already do this, if you want.

    // Always tell the Pushy-Library about the notification and its' content
    // So the Pushy-Library can post notifications internally
    [self.pushyLibrary didReceiveRemoteNotification:userInfo];
}
```

### Step 4.2: Launch-by-Notification ###
##### YourAppDelegate.m #####
* Extend `- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions` as following with the line `self.pushyLibrary.launchOptions = launchOptions;`:
``` objective-c
// Called on application-launch
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Initialize Pushy-Library on launch, very important!
    self.pushyLibrary = [BIPushy sharedInstance]; // Yep, this is a singleton. You might not like that, but it works.
    
	// Create a target, meaning here: a source of push notifications, a WordPress instance
	// You do not need to store this anywhere, the library makes it retrieveable by the readableIdentifier specified below
	BITarget *target1 = [[BITarget alloc] initWithBaseURL:@"http://my-wordpress-site-url.de"					// Your WordPress-Site's URL with http:// and without trailing-slash
																												// ATTENTION: Network requests might fail due App-Transport-Security if you do not use SSL.
																												// So create an exception to allow insecure calls to this URL, otherwise this whole thing will not work. Search-Keyword: NSAppTransportSecurity
																												// This will not be a problem if you use SSL. ATTENTION: Currently you can not use a different SSL-Port than 443.
										   andAPIEndPoint:@"wp-content/plugins/BIPushy/API/public/index.php/v1" // You won't change that
											 andAppSecret:@"my-very-secret-key"									// Set the target's App-Secret here
											  andTargetID:@"1"													// Set your target's ID here
									andReadableIdentifier:@"WP1"];												// You need to set something here to identify the source/target later, like "MyWordPress-1"
	
	
    self.pushyLibrary.launchOptions = launchOptions; // Pass the launchOptions to handle App-Launch by Push-Notification
 
  	// Add some notification-observers to count running network-requests. This gives us the opportunity to start/stop the NetworkIndicator to indicate that we're doing stuff
    // You should do this, too. But you might do this in a way that fits your application better. But this is for demo-reasons so you know which notifications you should observe for this.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(incrementNumberOfRunningNetworkRequestsByNotification:) name:BIPUSHY_DID_START_NETWORK_REQUEST_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(decrementNumberOfRunningNetworkRequestsByNotification:) name:BIPUSHY_DID_FINISH_NETWORK_REQUEST_NOTIFICATION object:nil];

 
    // Let the user select what he wants to subscribe to. You need to register for every target separately!
	[self.pushyLibrary doActionForTargets:@[@"WP1"] andAction:^(BITarget *target)
	{
		// Pass the user's preferences about alerts/bages/sounds.
		[target registerForNotificationsWithAlert:YES andBadge:YES andSound:YES];

	}];


    return YES;
}
``` 

### Step 5: Handle caught notifications ###

##### YourMainArticleOverViewViewController.h #####
* Import `BIPushy.h`

##### YourMainArticleOverViewViewController.m #####
* Create an instance-variable `@property (strong, nonatomic) NSMutableArray *dataArray;` which contains all of your articles/notifications. This depends on your data-structure and might be different for you.
* Implement `- (id)initWithCoder:(NSCoder *)decoder` as following:
``` objective-c
// Called very early on launch, much before viewDidLoad will be called
// Note: If we'll get a NF at this early point, we will not be able to update our views, because they don't exist!
// This is important to handle the case of our application beeing closed (not inactive) and being launched by a NF from NFC
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    
    if (self)
    {
        // Initialize DataStorage before (!) we start observing, important!
        self.dataArray = [NSMutableArray new];
        
        // Observe the NSNotificationCenter for PushNotifications during runtime
        [self setUpObservers];
    }
    
    return self;
}

```
* Implement `- (void)setUpObservers` for Notification Center observer setups, as following:
``` objective-c
- (void)setUpObservers
{
    // Register for PushNotifications of your choice. You probably want to handle different kinds of notifications different
    // E.g. You might want to handle New-Article-NF different than Updated-Article NF
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addPushByNotification:) name:BIPUSHY_DID_RECEIVE_ALL_NOTIFICATION object:nil];
}
```
* Implement `- (void)addPushByNotification:(NSNotification *)nf` as following to add a received notification to your data-structure und your UI. Keep in mind that this depends on your data-structure and your UI:
``` objective-c
// Called on new NF
- (void)addPushByNotification:(NSNotification *)nf
{
    // You really should check here whether we've got UI and if not, just add it to your DataStorage and handle displaying at line 45
    
    // Just keep in mind: This NF doesn't contain much information but the information you need to pull everything associated to the NF.
    // E.g. the title might be truncated and just a preview, don't use this in detail, just to tease the user!
    // The Article-ID is given, call WordPress (REST API) by this ID and retrieve every information you normally need for displaying the article.
    // So, for example, use the REST-API-retrieved title instead of the title provided by the NF.
    // To sum up, this might be the right point to load associated information about the article you need for displaying.
    
    // Add the new NF to the TableView
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:self.dataArray.count inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
    // Add the new NF to our DataStorage, this is important! You have to store at least the NotificationID of the NF-UserInfo-Object to pass it to the DetailViewController.
    [self.dataArray addObject:[nf userInfo]];
    [self.tableView endUpdates];
    
    // Special handling for NF that caused the application's current launch
    if ([[nf userInfo] objectForKey:@"IsLaunchPush"])
    {
        // Wait for 0.5 seconds before we push a new ViewController. You might want to increase this time until your current UI is completly loaded
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^
        {
            // Just push the DetailViewController to display the new article
            [self performSegueWithIdentifier:@"ShowPushDetails" sender:[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:(self.dataArray.count - 1) inSection:0]]];
        });
        
    }
    
}

```


### Step 6: Read receipt implementation for notifications ###

##### YourArticleDetailViewController.h #####
* Import `BIPushy.h`
* Create an instance-variable `@property NSDictionary *pushData;` which contains the notification-dictionary (passed by your YourMainArticleOverViewViewController)

##### YourArticleDetailViewController.m #####
* Implement this method-call in a method of your choice `- (void)markNotificationRead:(NSString *)notificationID`. You might want to wait a bit until you confirm the read receipt. But this is fully on your own how you define 'user read notification'. So put this line where ever you want, the following implementation waits just a second until it marks the notification as read:
``` objective-c
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    // After the article is visible to the user and he's able to read, you've to decide "when" a user has read a NF
    // For demo-reasons we define that after 1 second the user has read the NF
    // You might want to implement this in other ways and at other points. It's fully up to you, but keep in mind that this influences the stats.
    // Also take care of "manual" NF reads, which are not associated to articles!
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^
    {
        // Take sharedInstance of our AppDelegate's Pushy-Library and mark the current notification associated to the current article as read.
        // You don't need to track yourself whether the NF was previously marked as read, the server does this anyway.
        // This also lowers the Badge-Count automatically for you. Also it cancels all local notifications (clears NF-Center).
        [[[BIPushy sharedInstance] getTargetForReadableIdentifier:[self.pushData objectForKey:@"TargetID"] ] markNotificationRead:[self.pushData objectForKey:@"NotificationID"]];
    });

    
}

``` 

##### YourMainArticleOverViewViewController.m #####
* Before you push your YourArticleDetailViewController, you need to prepare it and pass the notification-dictionary. The following example uses segues, what you really should do, too. But this isn't an example that fits every kind of implementation you can have, so interpret this code and do this in a way that fits your setup.
``` objective-c
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Check whether we preparing a segue for our DetailViewController
    if ([segue.identifier isEqualToString:@"ShowPushDetails"])
    {
        NSIndexPath *indexPath = [self.tableView indexPathForCell:sender]; // Get the IndexPath of the currently tapped row
        BIPushDetailViewController *detailVC = [segue destinationViewController]; // Retrieve YourArticleDetailViewController-instance from the segue 
        detailVC.pushData = [self.dataArray objectAtIndex:indexPath.row]; // Assign the notification-dictionary to the pushData-property
    }
    
}
```

### Step 7: Actionable Notifications ###
Before we get started here, let's explain what this is all about: Since iOS8 we're blessed to interact with our Local/Remote-Notifcations. Notifications can offer buttons the user interacts with, so he can quickly respond to a notification, even without launching the app in foreground. This requires that your app registers for "kinds" of notifications that are interactable and register the buttons for these. Also the payload has to include (in the aps object) the category ("kind") of the current Push-Notification. Kind means that you have to say "notifications of kind x can display the buttons y and z". For our sample use-case the category is called `@"ARTICLE_ACTIONABLE"` and offers the buttons "mark-as-read" and "read". Means the user can mark an article as read from the notification. Example is here: he has read it some where else and just wants to "dismiss" the notification and also mark the article internally as read (we are goining to also mark the notification as read, as you should do this). We want to offer a second button, that just launches the app and displays the article. This is a functionallity we have anyway by just swiping the notification normally. So it's not going to be very difficult to understand and implement. But keep in mind: this is advanced functionallity and not required, it's just an advantage for the user.

##### YourAppDelegate.m #####
* Start by defining these constants at the beginning of the file
``` objective-c
// Define the constants you need to use actionable notifications
NSString *const NotificationCategoryIdentifier  = @"ARTICLE_ACTIONABLE"; // You should keep this very short, because it has to be included in every payload
NSString *const NotificationActionReadIdentifier = @"ACTION_READ";
NSString *const NotificationActionMarkReadIdentifier = @"ACTION_MARK_AS_READ";
```

* Implement `- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler` to respond to pushed buttons, as following:
``` objective-c
// Called if the user pushed a button in an actionable notification
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler
{
    // Always tell the Pushy-Library about the notification and its' content
    // So the Pushy-Library can post notifications internally
    // We are able to get the parsed Notification-Object directly
    NSDictionary *parsedNotifcation = [self.pushyLibrary didReceiveRemoteNotification:userInfo];

    if ([parsedNotifcation[@"Category"] isEqualToString:NotificationCategoryIdentifier])
    {
        if([identifier isEqualToString:NotificationActionReadIdentifier])
        {
            // You might not reach this point (in time), because the call in line 76 fires local notifications, so your App is doing things like it's implemented for App-launch by a Push-Notification
        }
        else if ([identifier isEqualToString:NotificationActionMarkReadIdentifier])
        {
            // Here you should put all the code that's required to just mark the article as read. Keep in mind, you've just seconds to do this, because it's running in Background

            // At first, we need to tell the library, that we're running minimal, because we're on background-mode
            [[BIPushy sharedInstance] setAppRunningMode:BIAppRunningModeMinimal];

            // Take sharedInstance of our YourAppDelegate's Pushy-Library and mark the current notification associated to the current article as read.
            // You don't need to track yourself whether the NF was previously marked as read, the server does this anyway.
            // This also lowers the Badge-Count automatically for you. Also it cancels all local notifications (clears NF-Center).
            [[BIPushy sharedInstance] markNotificationRead:[parsedNotifcation objectForKey:@"NotificationID"]];
        }
    
    }


    // Do not forget the CompletionHandler!
    if (completionHandler)
        completionHandler();

}
```
* Implement `- (NSSet *)getDefaultActionableNotifications` as a helper method to set up categories ("notification kinds") and their actionable-buttons, as following:
``` objective-c
// This is just a helper method to set up your actionable Notifications
- (NSSet *)getDefaultActionableNotifications
{
    // Define ReadAction
    UIMutableUserNotificationAction *readAction = [[UIMutableUserNotificationAction alloc] init];
    [readAction setActivationMode:UIUserNotificationActivationModeForeground]; // To read the Article, we need to launch the app in foreground (makes sense, yeha?).
    [readAction setTitle:@"Read"]; // You might want to localize this
    [readAction setIdentifier:NotificationActionReadIdentifier]; // Pass our identifier
    [readAction setDestructive:NO]; // We don't want this button red
    [readAction setAuthenticationRequired:YES]; // Because we actualy need to launch the app in foreground

    // Define MarkAsReadAction
    UIMutableUserNotificationAction *markReadAction = [[UIMutableUserNotificationAction alloc] init];
    [markReadAction setActivationMode:UIUserNotificationActivationModeBackground]; // We can do this in BackgroundMode.
    [markReadAction setTitle:@"Mark Read"]; // You might want to localize this
    [markReadAction setIdentifier:NotificationActionMarkReadIdentifier]; // Pass our identifier
    [markReadAction setDestructive:YES]; // Do we want this button red? Decide yourself.
    [markReadAction setAuthenticationRequired:NO]; // Because we just mark it internally and remote as read, if you need FileAccess or so, you might want to change this

    // Put the actions into a category
    UIMutableUserNotificationCategory *artcileActionCategory = [[UIMutableUserNotificationCategory alloc] init];
    [artcileActionCategory setIdentifier:NotificationCategoryIdentifier];
    [artcileActionCategory setActions:@[readAction, markReadAction] forContext:UIUserNotificationActionContextDefault];

    // Return it as a NSSet
    return [NSSet setWithObject:artcileActionCategory];
}
```
* Extend `- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions` as following with the line `NSSet *categories = [self getDefaultActionableNotifications];` and change `[self.pushyLibrary registerForNotificationsWithAlert:YES andBadge:YES andSound:YES];` to `[self.pushyLibrary registerForNotificationsWithAlert:YES andBadge:YES andSound:YES andCategories:categories];`:
``` objective-c
// Called on application-launch
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    // Initialize Pushy-Library on launch, very important!
    self.pushyLibrary = [BIPushy sharedInstance]; // Yep, this is a singleton. You might not like that, but it works.
    
	// Create a target, meaning here: a source of push notifications, a WordPress instance
	// You do not need to store this anywhere, the library makes it retrieveable by the readableIdentifier specified below
	BITarget *target1 = [[BITarget alloc] initWithBaseURL:@"http://my-wordpress-site-url.de"					// Your WordPress-Site's URL with http:// and without trailing-slash
																												// ATTENTION: Network requests might fail due App-Transport-Security if you do not use SSL.
																												// So create an exception to allow insecure calls to this URL, otherwise this whole thing will not work. Search-Keyword: NSAppTransportSecurity
																												// This will not be a problem if you use SSL. ATTENTION: Currently you can not use a different SSL-Port than 443.
										   andAPIEndPoint:@"wp-content/plugins/BIPushy/API/public/index.php/v1" // You won't change that
											 andAppSecret:@"my-very-secret-key"									// Set the target's App-Secret here
											  andTargetID:@"1"													// Set your target's ID here
									andReadableIdentifier:@"WP1"];												// You need to set something here to identify the source/target later, like "MyWordPress-1"
	
	
    self.pushyLibrary.launchOptions = launchOptions; // Pass the launchOptions to handle App-Launch by Push-Notification
 
 	// Add some notification-observers to count running network-requests. This gives us the opportunity to start/stop the NetworkIndicator to indicate that we're doing stuff
    // You should do this, too. But you might do this in a way that fits your application better. But this is for demo-reasons so you know which notifications you should observe for this.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(incrementNumberOfRunningNetworkRequestsByNotification:) name:BIPUSHY_DID_START_NETWORK_REQUEST_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(decrementNumberOfRunningNetworkRequestsByNotification:) name:BIPUSHY_DID_FINISH_NETWORK_REQUEST_NOTIFICATION object:nil];

    // Get your categories by the helper method
    NSSet *categories = [self getDefaultActionableNotifications];
 
    // Let the user select what he wants to subscribe to. You need to register for every target separately!
	[self.pushyLibrary doActionForTargets:@[@"WP1"] andAction:^(BITarget *target)
	{
		// Pass the user's preferences about alerts/bages/sounds and, if given, actionable-categories.
		[target registerForNotificationsWithAlert:YES andBadge:YES andSound:YES andCategories:categories];

	}];


    return YES;
}
``` 

### Step 8: Meta-Data Usage ###
Meta-Data is a powerfull feature to store/retrieve some data about your current device. In general, it works like a simple Key-Value-Storage on your server. The storage is located on the remote server, this gives us the oppertunity to interact with the Meta-Data on server-side. For example, you can set Push-Subscription options through Meta-Data. The server now respects Push-Subscription options you gave for the sepcific device. 
You can set a value for a specific unique key. Later, you can retreive the value by asking for it with the given key. Strings and Integers work fine, you should use them only. Stick to simple data-structures, because more complex structures may corrupt the Meta-Data for the server. But in theory, every data-structure that converts to JSON is possible.

##### YourAppDelegate.m #####
* Extend `- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken` as following:
``` objective-c
// Called if the register-process was successfull and we've got a DeviceToken
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // Always tell the Pushy-Library about the DeviceToken
    // Pushy-Library will take care about server-registration etc.
    [self.pushyLibrary didRegisterForRemoteNotificationsWithDeviceToken:deviceToken withCallback:^()
     {
		 // We need to refer to a specific target for the further actions
		 BITarget *WP1 = [self.pushyLibrary getTargetForReadableIdentifier:@"WP1"];
		 
         // You've to wait for Meta-Data, it becomes available in this callback. It's not any earlier available.
         // So, don't start with Meta-Operations until this callback!
         
         // Here you can see an example for meta usage
         NSString *deviceName = [[UIDevice currentDevice] name];
         
         // If the remote DeviceName needs to be updated…
         if([[WP1 getMetaValueForKey:@"device_name"] isEqualToString:@""] || ![[WP1 getMetaValueForKey:@"device_name"] isEqualToString:deviceName])
         {
             // … update the DeviceName
             [WP1 setMetaValue:deviceName forKey:@"device_name"];
         }
         
         // See what we've got now
         NSLog(@"Device Name: %@", [WP1 getMetaValueForKey:@"device_name"]);
         
         
         // Repeat this with the DeviceOS, just as another example
         NSString *deviceOS = [[UIDevice currentDevice] systemVersion];
         
         if([[WP1 getMetaValueForKey:@"device_os"] isEqualToString:@""] || ![[WP1 getMetaValueForKey:@"device_os"] isEqualToString:deviceName])
         {
             // … update the DeviceOS
             [WP1 setMetaValue:deviceOS forKey:@"device_os"];
         }
         
         // See what we've got now
         NSLog(@"Device OS: %@", [WP1 getMetaValueForKey:@"device_os"]);
         
    }];

}
``` 
_NOTE:_ This is just more a nonsense example, it is just an example and does not bring you anywhere. See this as example and use it for your purposes in your way. Please keep in mind that Meta-Data gets available if the block gets called. Until the block is executed, there is no Meta-Data available. Maybe, someday someone uses `deivce_name` and `device_os` to create some statistics extension, who knows? 


### Step 9: UserID Usage ###
The UserID field brings Meta-Data to a whole new level: By assigning two devices the same UserID, Push-Subscription options are respected for every device of a user. UserID means, you associate one or more devices to a user. For example, a user installed this app on his iPhone and his iPad. The user is (somehow) logged in with his account on both devices (what ever you define as 'account'). Now, right after login, you have assigned the UserID to the current device. For example, the User now makes a decision to subscribe a specific article's comments. You internally updated the Meta-Data of the current device as 'user subscribed article x for comments'. The server respects this Push-Subscription option for every device that is associated with the same UserID as the current device. Means, the User gets his Push-Notifications about the comments of article x on both devices instead of just the current device.
You should consider this as a feature, this is great and Apple recommends this, too. So with just a very simple call to the library, the user gets an amazing experience.

_NOTE:_ This works with intergers only. So you've to choose a hash without characters or use the UserID from your database (if given).

##### YourAppDelegate.m #####
* Extend `- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken` as following:
``` objective-c
// Called if the register-process was successfull and we've got a DeviceToken
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    // Always tell the Pushy-Library about the DeviceToken
    // Pushy-Library will take care about server-registration etc.
    [self.pushyLibrary didRegisterForRemoteNotificationsWithDeviceToken:deviceToken withCallback:^()
     {
		 // We need to refer to a specific target for the further actions
		 BITarget *WP1 = [self.pushyLibrary getTargetForReadableIdentifier:@"WP1"];
		 
         // You've to wait for Meta-Data, it becomes available in this callback. It's not any earlier available.
         // So, don't start with Meta-Operations until this callback!
         
         // Here you can see an example for meta usage
         NSString *deviceName = [[UIDevice currentDevice] name];
         
         // If the remote DeviceName needs to be updated…
         if([[WP1 getMetaValueForKey:@"device_name"] isEqualToString:@""] || ![[WP1 getMetaValueForKey:@"device_name"] isEqualToString:deviceName])
         {
             // … update the DeviceName
             [WP1 setMetaValue:deviceName forKey:@"device_name"];
         }
         
         // See what we've got now
         NSLog(@"Device Name: %@", [WP1 getMetaValueForKey:@"device_name"]);
         
         
         // Repeat this with the DeviceOS, just as another example
         NSString *deviceOS = [[UIDevice currentDevice] systemVersion];
         
         if([[WP1 getMetaValueForKey:@"device_os"] isEqualToString:@""] || ![[WP1 getMetaValueForKey:@"device_os"] isEqualToString:deviceName])
         {
             // … update the DeviceOS
             [WP1 setMetaValue:deviceOS forKey:@"device_os"];
         }
         
         // See what we've got now
         NSLog(@"Device OS: %@", [WP1 getMetaValueForKey:@"device_os"]);
         
         
         // Now an example that shows the basic-usage of the UserID
         // You should put this whole "set UserID code" somewhere else.
         // Eg. after a login for comments or anything that works similar.
         // You might use your own Login-System to get the unique ID
         // Otherwise: You could just hash the e-mail address he provided for comments
         // Note: This works only with integers
         
         // Check whether we've got already an UserID
         if([WP1 getUserID] == 0)
         {
             // Our User wasn't logged in, so he had no UserID.
             // But now, he's logged in and we set his UserID.
             [WP1 setUserID:42];
         }
         
     }];

}

``` 

_NOTE:_ 42 is just a placeholder, you have to pass your generated UserID, however you generated it. I recommend to use the UserID of your Login-System (if complex app) or hash the mail address as integer only.

### Step 10: Comment-Push ###
With Comment-Push you can subscribe a user to comments of an article. It takes advantage of the Meta-Data feature and the UserID feature. The server automatically generates separate Push-Notifications for the comments, if a user subscribed them. After leaving a comment, a user should automatically be subscribed to the Push-Notifications of the commented article (this is your part). Do not subscribe a user automatically to articles. Just to articles he either commented or specifically wanted to be subscribed to. It is recommended to put a switch under each article to manage Comment-Subscription of an article. In addition, you should put a global switch into your settings section, to automatically disable all Comment-Push-Notifications. By respecting the UserID feature, you automatically set the Push-Subsription options to all devices of a user.
The server uses a different Payload-Template for Comment-Push-Notifications, which enables us to use different actionable Notifications for comments. Think about a "Reply" and a "Mute" button.

### Step 10.1: Subscription ###
This paragraph is about the Comment-Push-Subscription per article. The user should be automatically subscribed if he leaves a comment.

_The Meta-Data Key is structured as following: `subscribes_comment_{WPPostID}` (you have to replace `{WPPostID}` with the actual WordPress-PostID). The value is an integer 0/1 and means off/on (you can use a boolean too)._

##### YourArticleDetailViewController.h #####
* Create an instance-variable `@property (weak, nonatomic) IBOutlet UISwitch *subscribeCommentsSwitch;` and connect it to an UISwitch Object in your Storyboard-View as IBOutlet. This switch represents the Comment-Subscription-Status of the current article
* Create a method `- (IBAction)pushSubscribeCommentsSwitch:(id)sender;` that gets called if the `subscribeCommentsSwitch` is pushed. Connect it to the UISwitch Object in your Storyboard-View as IBAction.


##### YourArticleDetailViewController.m #####
* Implement `- (IBAction)pushSubscribeCommentsSwitch:(id)sender;`
``` objective-c
- (IBAction)pushSubscribeCommentsSwitch:(id)sender
{
    // Build the MetaKey by the scheme: subscribes_comment_x, x is the PostID
    NSString *metaKey = [NSString stringWithFormat:@"subscribes_comment_%@", [self.pushData objectForKey:@"PostID"]];
    // Tell the library about the user's preferences
    [[[BIPushy sharedInstance] getTargetForReadableIdentifier:[self.pushData objectForKey:@"TargetID"] ] setMetaValue:[NSNumber numberWithBool:[self.subscribeCommentsSwitch isOn]] forKey:metaKey];
}
```

* Extend `- (void)viewDidLoad` as following to load the inital Comment-Subscription-Status:
``` objective-c
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    // Start observing for incoming notifications
    [self setUpObservers];
    
    // This is just some UI stuff for demo-reasons
    self.title = [self.pushData objectForKey:@"PostID"];
    self.pushTitle.text = [self.pushData objectForKey:@"Title"];
	//self.pushAlert.text = [self.pushData objectForKey:@"Alert"];
	self.pushAlert.text = [NSString stringWithFormat:@"%@ (From Target-ID/Readable-Identifier: %@)",[self.pushData objectForKey:@"Alert"], [self.pushData objectForKey:@"TargetID"]];
    
	// Check whether subscription for comments of this post are enabled
    NSString *metaKey = [NSString stringWithFormat:@"subscribes_comment_%@", [self.pushData objectForKey:@"PostID"]];
    NSInteger metaValue = [[[[BIPushy sharedInstance] getTargetForReadableIdentifier:[self.pushData objectForKey:@"TargetID"] ] getMetaValueForKey:metaKey] integerValue];
    [self.subscribeCommentsSwitch setOn:metaValue animated:YES];

    // After the article is visible to the user and he's able to read, you've to decide "when" a user has read a NF
    // For demo-reasons we define that after 1 second the user has read the NF
    // You might want to implement this in other ways and at other points. It's fully up to you, but keep in mind that this influences the stats.
    // Also take care of "manual" NF reads, which are not associated to articles!
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^
    {
        // Take sharedInstance of our AppDelegate's Pushy-Library and mark the current notification associated to the current article as read.
        // You don't need to track yourself whether the NF was previously marked as read, the server does this anyway.
        // This also lowers the Badge-Count automatically for you. Also it cancels all local notifications (clears NF-Center).
        [[[BIPushy sharedInstance] getTargetForReadableIdentifier:[self.pushData objectForKey:@"TargetID"] ] markNotificationRead:[self.pushData objectForKey:@"NotificationID"]];
    });
    
}
```


### Step 10.2: Global Subscription ###
This paragraph is about the Comment-Push-Subscription that disables all Comment-Push-Notifications. This is a global setting, so it does not interferer with the Comment-Push-Subscription per article. It works like a kill-switch, if a user is annoyed by Comment-Push-Notifications. 
_NOTE:_ Turning the switch on does not mean the user is subscribed to all articles but to the articles he subscribed earlier.

_The Meta-Data Key is: `subscribes_comments`. The value is an integer 0/1 and means off/on (you can use a boolean too)._

##### YourSettingsViewController.h #####
* Create an instance-variable `@property (weak, nonatomic) IBOutlet UISwitch *subscribeCommentsSwitch;` and connect it to an UISwitch Object in your Storyboard-View as IBOutlet. This switch represents the global Comment-Subscription-Status.
* Create an instance-variable `@property (strong, nonatomic) BITarget *WP1;` to store the instance of the current target
* Create a method `- (IBAction)pushSubscribeCommentsSwitch:(id)sender;` that gets called if the `subscribeCommentsSwitch` is pushed. Connect it to the UISwitch Object in your Storyboard-View as IBAction.


##### YourSettingsViewController.m #####
* Implement `- (IBAction)pushSubscribeCommentsSwitch:(id)sender;`
``` objective-c
// You always should provide this option for any user
- (IBAction)pushSubscribeCommentsSwitch:(id)sender
{
    if(self.subscribeCommentsSwitch.isOn)
    {
        // Turn global comments push on. The key is always 'subscribes_comments'!
		[self.WP1 setMetaValue:[NSNumber numberWithBool:YES] forKey:@"subscribes_comments"];
    }
    else
    {
        // User does not want push for comments, turn it off. The key is always 'subscribes_comments'!
        [self.WP1 setMetaValue:[NSNumber numberWithBool:NO] forKey:@"subscribes_comments"];
    }
}
```

* Extend `- (void)viewDidLoad` as following to load the inital global Comment-Subscription-Status:
``` objective-c
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Just to dismiss the Keyboard
    UITapGestureRecognizer *tapToDismiss = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissNumberKeyBoard)];
    [self.view addGestureRecognizer:tapToDismiss];
	
	// Store the WP1 target as demo and make further usage more readable
	self.WP1 = [[BIPushy sharedInstance] getTargetForReadableIdentifier:@"WP1"];
	
    // Set UserID
    self.userIDTextField.text = [NSString stringWithFormat:@"%ld", (long)[self.WP1 getUserID]];
    
    // Set global Comment-Subscription switch. The key is always 'subscribes_comments'!
    [self.subscribeCommentsSwitch setOn:[[self.WP1 getMetaValueForKey:@"subscribes_comments"] integerValue] animated:YES];
}
```

### Step 10.3: Interactive Comment-Push ###
We have to add a new category of actionable Push-Notifications and add the buttons, for example "Reply" and "Mute". The actions are very simple in general. "Reply" is a foreground action (this might change with upcoming iOS releases), so we need to launch the app. "Mute" is a background action and can be done without starting the app. "Mute" should apply to the current article's Comment-Push-Notifications only!

##### YourAppDelegate.m #####
* Extend the Constants-Section on top of the file `#pragma mark - Constants` as following:
``` objective-c
// Define the constants you need to use actionable notifications
NSString *const NotificationPostCategoryIdentifier  = @"ARTICLE_ACTIONABLE"; // You should keep this very short, because it has to be included in every payload
NSString *const NotificationPostActionReadIdentifier = @"ACTION_READ";
NSString *const NotificationPostActionMarkReadIdentifier = @"ACTION_MARK_AS_READ";

NSString *const NotificationCommentCategoryIdentifier  = @"COMMENT_ACTIONABLE"; // You should keep this very short, because it has to be included in every payload
NSString *const NotificationCommentActionReplyIdentifier = @"ACTION_REPLY";
NSString *const NotificationCommentActionMuteIdentifier = @"ACTION_MUTE";

```
* Extend `- (NSSet *)getDefaultActionableNotifications` as following:
``` objective-c
// This is just a helper method to set up your actionable Notifications
- (NSSet *)getDefaultActionableNotifications
{
    // Normal Article Push
    
    
    // Define ReadAction
    UIMutableUserNotificationAction *readAction = [[UIMutableUserNotificationAction alloc] init];
    [readAction setActivationMode:UIUserNotificationActivationModeForeground]; // To read the Article, we need to launch the app in foreground (makes sense, yeha?).
    [readAction setTitle:@"Read"]; // You might want to localize this
    [readAction setIdentifier:NotificationPostActionReadIdentifier]; // Pass our identifier
    //[readAction setDestructive:NO]; // We don't want this button red
    [readAction setAuthenticationRequired:YES]; // Because we actualy need to launch the app in foreground
    
    // Define MarkAsReadAction
    UIMutableUserNotificationAction *markReadAction = [[UIMutableUserNotificationAction alloc] init];
    [markReadAction setActivationMode:UIUserNotificationActivationModeBackground]; // We can do this in BackgroundMode.
    [markReadAction setTitle:@"Mark Read"]; // You might want to localize this
    [markReadAction setIdentifier:NotificationPostActionMarkReadIdentifier]; // Pass our identifier
    [markReadAction setDestructive:YES]; // Do we want this button red? Decide yourself.
    [markReadAction setAuthenticationRequired:NO]; // Because we just mark it internally and remote as read, if you need FileAccess or so, you might want to change this
    
    // Put the actions into a category
    UIMutableUserNotificationCategory *artcileActionCategory = [[UIMutableUserNotificationCategory alloc] init];
    [artcileActionCategory setIdentifier:NotificationPostCategoryIdentifier];
    [artcileActionCategory setActions:@[readAction, markReadAction] forContext:UIUserNotificationActionContextDefault];
    
    
    // Comment Push
    UIMutableUserNotificationAction *replyAction = [[UIMutableUserNotificationAction alloc] init];
    [replyAction setActivationMode:UIUserNotificationActivationModeForeground]; // To show the Comments, we need to launch the app in foreground (makes sense, yeha?)
    [replyAction setTitle:@"Reply"]; // You might want to localize this
    [replyAction setIdentifier:NotificationCommentActionReplyIdentifier]; // Pass our identifier
    [replyAction setDestructive:NO]; // We don't want this button red
    [replyAction setAuthenticationRequired:YES]; // Because we actualy need to launch the app in foreground
    
    // Define Mute Thread Action
    UIMutableUserNotificationAction *muteThreadCommentsAction = [[UIMutableUserNotificationAction alloc] init];
    [muteThreadCommentsAction setActivationMode:UIUserNotificationActivationModeBackground]; // We can do this in BackgroundMode.
    [muteThreadCommentsAction setTitle:@"Mute"]; // You might want to localize this
    [muteThreadCommentsAction setIdentifier:NotificationCommentActionMuteIdentifier]; // Pass our identifier
    [muteThreadCommentsAction setDestructive:YES]; // Do we want this button red? Decide yourself.
    [muteThreadCommentsAction setAuthenticationRequired:NO]; // Because we just mark it internally and remote as read, if you need FileAccess or so, you might want to change this
    
    
    // Put the actions into a category
    UIMutableUserNotificationCategory *commentActionCategory = [[UIMutableUserNotificationCategory alloc] init];
    [commentActionCategory setIdentifier:NotificationCommentCategoryIdentifier];
    [commentActionCategory setActions:@[muteThreadCommentsAction, replyAction] forContext:UIUserNotificationActionContextDefault];
    
    
    
    // Return it as a NSSet
    return [NSSet setWithObjects:artcileActionCategory, commentActionCategory, nil];
}
```

* Extend `- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler` as following:
``` objective-c
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void (^)())completionHandler
{
    // Always tell the Pushy-Library about the notification and its' content
    // So the Pushy-Library can post notifications internally
    // We are able to get the parsed Notification-Object directly
    NSDictionary *parsedNotifcation = [self.pushyLibrary didReceiveRemoteNotification:userInfo];
    
    // We need to check whether the target mapping worked!
	if(parsedNotifcation != nil)
	{
		// You always need to refer to a specific target
		// We can retrieve it from the notification itself and automatically do the action for the right target
		BITarget *currentTarget = [self.pushyLibrary getTargetForReadableIdentifier:[parsedNotifcation objectForKey:@"TargetID"]];
		
		if ([parsedNotifcation[@"Category"] isEqualToString:NotificationPostCategoryIdentifier])
		{
			if([identifier isEqualToString:NotificationPostActionReadIdentifier])
			{
				// You might not reach this point (in time), because the call in line 70 fires local notifications, so your App is doing things like it's implemented for App-launch by a Push-Notification
			}
			else if ([identifier isEqualToString:NotificationPostActionMarkReadIdentifier])
			{
				// Here you should put all the code that's required to just mark the article as read. Keep in mind, you've just seconds to do this, because it's running in Background
				
				// At first, we need to tell the library, that we're running minimal, because we're on background-mode
				[[BIPushy sharedInstance] setAppRunningMode:BIAppRunningModeMinimal];
				
				// Take sharedInstance of our AppDelegate's Pushy-Library and mark the current notification associated to the current article as read.
				// You don't need to track yourself whether the NF was previously marked as read, the server does this anyway.
				// This also lowers the Badge-Count automatically for you. Also it cancels all local notifications (clears NF-Center).
				[currentTarget markNotificationRead:[parsedNotifcation objectForKey:@"NotificationID"]];
			}
		}
		else if ([parsedNotifcation[@"Category"] isEqualToString:NotificationCommentCategoryIdentifier])
		{
			if([identifier isEqualToString:NotificationCommentActionReplyIdentifier])
			{
				// The user wants to reply to the given comment, this is a foreground action!
				// So open the article and show the comments, jump directly into the TextField!
				// This is your task
			}
			else if ([identifier isEqualToString:NotificationCommentActionMuteIdentifier])
			{
				// The user does not want any Pushs for this article's comments anylonger.
				
				// Here you should put all the code that's required to mute the comments of the related post. Keep in mind, you've just seconds to do this, because it's running in Background
				
				// At first, we need to tell the library, that we're running minimal, because we're on background-mode
				[[BIPushy sharedInstance] setAppRunningMode:BIAppRunningModeMinimal];
				
				// Now, disable the comment push of the article
				NSString *metaKey = [NSString stringWithFormat:@"subscribes_comment_%@", [parsedNotifcation objectForKey:@"PostID"]];
				[currentTarget setMetaValue:[NSNumber numberWithBool:NO] forKey:metaKey];
				
				// You might want to mark this Notification as read
				[currentTarget markNotificationRead:[parsedNotifcation objectForKey:@"NotificationID"]];
			}
			
		}
		
	}
	
	
    // Do not forget about the CompletionHandler!
    if (completionHandler)
        completionHandler();
        
}
```

### Step 10.4: Comment-Push Quick-Reply ###
Quick-Reply enables, since iOS9, also 3rd Apps to directly respond to a notification by text. The user does not need to launch your app, this works from Lockscreen and Notification Center. You should consider whether the Passcode should be entered before replying.

If you actually want to make Quick-Reply a feature: read carefully. Yes, Quick-Reply is a time-safer and makes things a lot easier for users.
BUT we've got a problem. Quick-Reply does only make sense if a user can read the most part of the content he wants to respond to. Compare this toiMessage, you can't give a real response if you're only able to read the first seven words of the message. So the user needs to be able to read the biggest part of the message. Apple automatically truncates the part of the message that can not be viewed on the device, that's pretty helpful. To come to the real problem: Whether this is a problem to you, depends on your deployment target. Since Apple decided to support different sizes of payloads depending on the OS, we're in trouble:

* iOS9: 4KB (currently undocumented)
* iOS8: 2KB (this value is taken from APNs documentation)
* iOS7 and prior: 256B

What does this mean? This means you've to know your target-audience:
* If you're only supporting iOS9+, you can go up to 4096 Bytes.
* If you're only supporting iOS8+, you can go up to 2048 Bytes.
* If you're supporting iOS7+, you can not go higher than 256 Bytes.

Depending on your custom-payload-configuration, 2048 Bytes will work for a nice Quick-Reply experience. 256 Bytes mean, you should not use Quick-Reply or drop iOS7 support.

BIPushy REST-API can only deal with one setting for all. So it's a problem and it's currently nearly impossible to implement this depending on the device OS. There might be ways, but at this time I'm not going to support this.

If you've chosen your target-audience 256/2048/4096 Bytes, you can change the value of AVAILABLE_PAYLOAD_BYTES in line 23 wp-content/plugins/BIPushy/API/config.php
Default value is 256 Bytes. I'm maybe going to put this in settings, so it will not be overwritten during updates.

##### YourAppDelegate.m #####

* Implement `- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo withResponseInfo:(NSDictionary *)responseInfo completionHandler:(void (^)())completionHandler;`
``` objective-c
// This method is called if the user returned text from a quick-reply remote-notification (ATTENTION: AVAILABLE SINCE IOS9!!)
- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo withResponseInfo:(NSDictionary *)responseInfo completionHandler:(void (^)())completionHandler
{
	// You always need to refer to a specific target
	BITarget *WP1 = [self.pushyLibrary getTargetForReadableIdentifier:@"WP1"];

    // Always tell the Pushy-Library about the notification and its' content
    // So the Pushy-Library can post notifications internally
    // We are able to get the parsed Notification-Object directly
    NSDictionary *parsedNotifcation = [WP1 didReceiveRemoteNotification:userInfo];
    
    if ([parsedNotifcation[@"Category"] isEqualToString:NotificationCommentCategoryIdentifier])
    {
        if([identifier isEqualToString:NotificationCommentActionReplyIdentifier])
        {
            // At first, we need to tell the library, that we're running minimal, because we're on background-mode
            [[BIPushy sharedInstance] setAppRunningMode:BIAppRunningModeMinimal];
            
            // Just for the stats
            [WP1 markNotificationRead:[parsedNotifcation objectForKey:@"NotificationID"]];
            
            NSString *comment = responseInfo[UIUserNotificationActionResponseTypedTextKey];
            
            if (comment.length > 0)
            {
                // Your Server-Interaction-Code should be placed here
            }
            
        }
        
    }
    
    
    // Do not forget about the CompletionHandler!
    if (completionHandler)
        completionHandler();
    
}
```

* Extend `- (NSSet *)getDefaultActionableNotifications` as following:
``` objective-c
// This is just a helper method to set up your actionable Notifications
- (NSSet *)getDefaultActionableNotifications
{
    // Normal Article Push
    
    
    // Define ReadAction
    UIMutableUserNotificationAction *readAction = [[UIMutableUserNotificationAction alloc] init];
    [readAction setActivationMode:UIUserNotificationActivationModeForeground]; // To read the Article, we need to launch the app in foreground (makes sense, yeha?).
    [readAction setTitle:@"Read"]; // You might want to localize this
    [readAction setIdentifier:NotificationPostActionReadIdentifier]; // Pass our identifier
    //[readAction setDestructive:NO]; // We don't want this button red
    [readAction setAuthenticationRequired:YES]; // Because we actualy need to launch the app in foreground
    
    // Define MarkAsReadAction
    UIMutableUserNotificationAction *markReadAction = [[UIMutableUserNotificationAction alloc] init];
    [markReadAction setActivationMode:UIUserNotificationActivationModeBackground]; // We can do this in BackgroundMode.
    [markReadAction setTitle:@"Mark Read"]; // You might want to localize this
    [markReadAction setIdentifier:NotificationPostActionMarkReadIdentifier]; // Pass our identifier
    [markReadAction setDestructive:YES]; // Do we want this button red? Decide yourself.
    [markReadAction setAuthenticationRequired:NO]; // Because we just mark it internally and remote as read, if you need FileAccess or so, you might want to change this
    
    // Put the actions into a category
    UIMutableUserNotificationCategory *artcileActionCategory = [[UIMutableUserNotificationCategory alloc] init];
    [artcileActionCategory setIdentifier:NotificationPostCategoryIdentifier];
    [artcileActionCategory setActions:@[readAction, markReadAction] forContext:UIUserNotificationActionContextDefault];
    
    
    // Comment Push
    UIMutableUserNotificationAction *replyAction = [[UIMutableUserNotificationAction alloc] init];
    [replyAction setTitle:@"Reply"]; // You might want to localize this
    [replyAction setIdentifier:NotificationCommentActionReplyIdentifier]; // Pass our identifier
    [replyAction setDestructive:NO]; // We don't want this button red
    
    // Check whether Quick-Reply is available (ATTENTION: AVAILABLE SINCE IOS9)
    if([replyAction respondsToSelector:@selector(setBehavior:)])
    {        
        [replyAction setActivationMode:UIUserNotificationActivationModeBackground]; // We can work in Background
        [replyAction setBehavior:UIUserNotificationActionBehaviorTextInput]; // We want Quick-Reply!
        [replyAction setAuthenticationRequired:NO];  // You might want to change this
    }
    else
    {
        [replyAction setActivationMode:UIUserNotificationActivationModeForeground]; // To show the Comments, we need to launch the app in foreground (makes sense, yeha?).
        [replyAction setAuthenticationRequired:YES]; // Because we actualy need to launch the app in foreground
    }

    
    // Define Mute Thread Action
    UIMutableUserNotificationAction *muteThreadCommentsAction = [[UIMutableUserNotificationAction alloc] init];
    [muteThreadCommentsAction setActivationMode:UIUserNotificationActivationModeBackground]; // We can do this in BackgroundMode.
    [muteThreadCommentsAction setTitle:@"Mute"]; // You might want to localize this
    [muteThreadCommentsAction setIdentifier:NotificationCommentActionMuteIdentifier]; // Pass our identifier
    [muteThreadCommentsAction setDestructive:YES]; // Do we want this button red? Decide yourself.
    [muteThreadCommentsAction setAuthenticationRequired:NO]; // Because we just mark it internally and remote as read, if you need FileAccess or so, you might want to change this
    
    
    // Put the actions into a category
    UIMutableUserNotificationCategory *commentActionCategory = [[UIMutableUserNotificationCategory alloc] init];
    [commentActionCategory setIdentifier:NotificationCommentCategoryIdentifier];
    [commentActionCategory setActions:@[muteThreadCommentsAction, replyAction] forContext:UIUserNotificationActionContextDefault];
    
    
    
    // Return it as a NSSet
    return [NSSet setWithObjects:artcileActionCategory, commentActionCategory, nil];
}

```


## Where can I grab libBIPushy.a? ##

This library is for commercial purposes and not part of this repository.

* Contact me at jan@lets-byte.it


## Found a bug or have a feature request? ##
* Write a ticket 


## Any questions left? ##

* Use our mailing list at pushy.ios@lets-byte.it